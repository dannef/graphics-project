# Description

This is a project based using code from TDA362 Computer Graphics course at Chalmers University.

Current implementations are:
 * Screen space reflections according to "Efficient Screen Space Ray Tracing" By Morgan Mcguire and Michael Mara.
 * Screen space ambient occlusion
 * Deferred rendering.
 * Not so fast tiled deferred renderer
 * Basic particle system