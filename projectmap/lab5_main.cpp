
#include <GL/glew.h>

// STB_IMAGE for loading images of many filetypes
#include <stb_image.h>

#include <algorithm>
#include <chrono>
#include <string>
#include <iostream>
#include <fstream>


#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>
using namespace glm;

#include <labhelper.h>
#include "ParticleSystem.h"
#include <imgui.h>
#include <imgui_impl_sdl_gl3.h>

#include <Model.h>
#include "hdr.h"

#include "tiling.h"
using std::min;
using std::max;

///////////////////////////////////////////////////////////////////////////////
// Various globals
///////////////////////////////////////////////////////////////////////////////
SDL_Window* g_window = nullptr;
float currentTime = 0.0f;
float ssrPassTime = 0.0f;
GLuint64 startTime, stopTime;
float lightpassTime = 0.0f;
float tileUploadTime = 0.0f;
float tileClassTime = 0.0f;
float tileClearingTime = 0.0f;

GLint64 timer;
vec3 hemiSphereSamples[16];
int colaZero = 20;
GLuint rotationTexture, light_color_texture, light_position_texture;

mat4 projectionMatrix;
mat4 viewMatrix;
mat4 mouse_view_matrix;

float texCoords[] = {
	1.0f, 1.0f, // topright
	1.0, 0.0f, // bottom right
	0.0f, 0.0f, // bottom left
	0.0f, 1.0f };// top left
///////////////////////////////////////////////////////////////////////////////
// Shader programs
///////////////////////////////////////////////////////////////////////////////
GLuint backgroundProgram, shaderProgram, postFxShader, SSAOShader, genGBufferShader, visualizeTestGBufferShader;
GLuint lightPassShader, blurShader, part1shader, simpleShader, partshader, lightingTiledShader;
///////////////////////////////////////////////////////////////////////////////
// Environment
///////////////////////////////////////////////////////////////////////////////
float environment_multiplier = 1.0f;
GLuint environmentMap, irradianceMap, reflectionMap;
const std::string envmap_base_name = "001";

///////////////////////////////////////////////////////////////////////////////
// Light sources
///////////////////////////////////////////////////////////////////////////////
float point_light_intensity_multiplier = 10000.0f;
vec3 point_light_color = vec3(0.9f, 0.8f, 0.7f);
const vec3 lightPosition = vec3(20.0f, 40.0f, 0.0f);
//const vec3 lightPosition = vec3(15.0f, 15.0f, -15.0f);

vec3 particlePositions[10000];
vec3 particleColors[1000];
vec3 lightPositions[100];
vec3 lightColors[200]; 
vec4 shader_dat[10000];


GLuint particleTexture;
GLuint ssbo_tile_rendering;
GLuint ssbo_particle_positions;
GLuint ssbo_particle_velocities;

// TODO: Create class, point light, 5
// create 100 light sources. all with random colors
// add uniforms to the light pass
// Create array of position vec3 for light pos
// create array of colors for lights.

int max_active_particles = 2;

ParticleSystem particle_system(max_active_particles);
int active_particles;
std::vector<glm::vec4> data2;
unsigned int VAO, VBO;


///////////////////////////////////////////////////////////////////////////////
// Compute Shader Ray tracing variables.
///////////////////////////////////////////////////////////////////////////////
GLuint tex_output;
GLuint ray_program;
GLuint part_compute_program;
///////////////////////////////////////////////////////////////////////////////
// Camera parameters.
///////////////////////////////////////////////////////////////////////////////
vec3 securityCamPos = vec3(0.0, 0.0, 100.0f);
vec3 securityCamDirection = normalize(vec3(0.0f) - securityCamPos);
//vec3 cameraPosition(15.0f, 413.0f, 855.0f);
vec3 cameraPosition(0.0f, 0.0f, 100.0f);
vec3 cameraDirection = normalize(vec3(0.0f) - cameraPosition);
float cameraSpeed = 2.3f;

vec3 worldUp(0.0f, 1.0f, 0.0f);

///////////////////////////////////////////////////////////////////////////////
// Models
///////////////////////////////////////////////////////////////////////////////
const std::string model_filename = "../scenes/cornell.obj";
//const std::string model_filename = "../scenes/NewShip.obj";
labhelper::Model *landingpadModel = nullptr;
labhelper::Model *fighterModel = nullptr;
labhelper::Model *sphereModel = nullptr;
labhelper::Model *cameraModel = nullptr;
labhelper::Model *cornellModel = nullptr;
labhelper::Model *planeModel = nullptr;
labhelper::Model *sponzaModel = nullptr;
///////////////////////////////////////////////////////////////////////////////
// Post processing effects
///////////////////////////////////////////////////////////////////////////////
enum PostProcessingEffect
{
	None = 0,
	Sepia = 1,
	Mushroom = 2,
	Blur = 3,
	Grayscale = 4,
	Composition = 5,
	Mosaic = 6,
	Separable_blur = 7,
	Bloom = 8
};

int currentEffect = PostProcessingEffect::None;
int filterSize = 1;
int filterSizes[12] = {3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25};
float radius[5] = { 0.5, 1.0, 3.0, 6.0, 9.0 };
int kernel_size[5] = { 8, 16, 24,32,64 };
int ssaoEffect = 1;
int showSSAO = 0;
int useTiled = 1;
int tile_size = 1;
int tile_sizes[5] = { 32, 48, 64, 86, 128 };

float point_light_radius = 15.0f;

float basic_ssr_trace_length = 2.0;
int basic_ssr_trace_steps = 40;
int basic_ssr_binary_steps = 3;
int basic_ssr_binary_search = 1;

int collisionBox = 1;
int vel = 1;
int ray_test_program_on = 0;
int particle_simulation_on = 0;
int repel = 0;
int stop_particles = 0;
vec3 attraction_point;

///////////////////////////////////////////////////////////////////////////////
// Tiling data
///////////////////////////////////////////////////////////////////////////////

std::vector<Tile> tiles;
std::vector<uint32_t> light_list;
std::vector<uint32_t> light_offset;
std::vector<uint32_t> light_size;

int TILE_WIDTH = 64;
int TILE_HEIGHT = 64;

int nr_of_lights = 10000;
// 1920x1080
// 16x16 int nr_of_tiles = 3680;

// 64x64
int nr_of_tiles = 240; // 64x64 on 1280x720
//int nr_of_tiles = 510 ;// 64x64 @ 1920x1080

///////////////////////////////////////////////////////////////////////////////
// custom generated textures
///////////////////////////////////////////////////////////////////////////////

GLuint createRandomRotations() {
	
	std::vector<glm::vec3> ssaoNoise;
	for (unsigned int i = 0; i < 16; i++)
	{
		// Generate a vector which is altered in the x and y components and simply add
		// them to the tangent space normal in order to rotate it. With this we do not need
		// any rotationmatrix.
		glm::vec3 noise(
			labhelper::randf() * 2.0 - 1.0,
			labhelper::randf() * 2.0 - 1.0,
			0.0f);
		ssaoNoise.push_back(noise);
	}

	GLuint noiseTexture;
	glGenTextures(1, &noiseTexture);
	glBindTexture(GL_TEXTURE_2D, noiseTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, 4, 4, 0, GL_RGB, GL_FLOAT, &ssaoNoise[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	
	return noiseTexture;
	
	/*
	std::vector<float> rotations;
	rotations.resize(4096);


	for (float &val : rotations) {
		val = (labhelper::randf() * 360.0f) / 360.0f;
	}

	GLuint angleTexture;
	glGenTextures(1, &angleTexture);
	glBindTexture(GL_TEXTURE_2D, angleTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_R8, 64, 64, 0, GL_RED, GL_FLOAT, &rotations[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	
	glBindTexture(GL_TEXTURE_2D, 0);
	return angleTexture;

	*/
}

GLuint gen_light_colors(int n) {

	std::vector<glm::vec3> light_sources;
	light_sources.resize(n);
	for (unsigned int i = 0; i < n; i++)
	{
		glm::vec3 light_color = vec3(labhelper::randf(), labhelper::randf(), labhelper::randf());
		//std::cout << light_color.x*0.1 << ", " << light_color.y*0.1 << ", " << light_color.z*0.1 << "\n";
		light_sources[i] = light_color*0.3f;
	}
	/*
	light_sources[0] = vec3(labhelper::randf(), labhelper::randf(), labhelper::randf())*0.1f;
	light_sources[1] = vec3(.0f, 1.0f, 0.0f);
	light_sources[2] = vec3(0.0f, 0.0f, 1.0f);
	light_sources[3] = vec3(.3f, 0.5f, 0.7f);
	light_sources[4] = vec3(1.0f, 1.0f, 1.0f);*/
	GLuint col_texture;
	glGenTextures(1, &col_texture);
	glBindTexture(GL_TEXTURE_2D, col_texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, n, 1, 0, GL_RGBA, GL_FLOAT, &light_sources[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	return col_texture;
}

GLuint gen_light_positions(vec3 lights[], int n) {

	std::vector<glm::vec3> light_positions;
	light_positions.resize(n);

	//light_positions[0] = lights[0];
	GLuint pos_texture;
	glGenTextures(1, &pos_texture);
	glBindTexture(GL_TEXTURE_2D, pos_texture);
	//glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, n, 1, 0, GL_RGB, GL_FLOAT, nullptr);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, n, 1, 0, GL_RGBA, GL_FLOAT, nullptr);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	return pos_texture;
}
void update_light_positions_texture(vec3 lights[], int n) {

	std::vector<glm::vec3> light_positions;
	light_positions.resize(n);
	for (unsigned int i = 0; i < n; i++)
	{
		// Positions are normalized to 0 and 1 when they are sent to GPU
		// we could project them, then do the inverse projection on the GPU.

		vec4 l = projectionMatrix * viewMatrix * vec4(lights[i], 1.0f);
		l /= l.w;
		l.x = (l.x + 1.0) *0.5;
		l.y = (l.y + 1.0) *0.5;
		l.z = (l.z + 1.0) *0.5;

		light_positions[i] = vec3(l);
	}

	glBindTexture(GL_TEXTURE_2D, light_position_texture);
	//glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 5, 1, GL_RGB, GL_FLOAT, &light_positions[0]);
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, n, 1, GL_RGB, GL_FLOAT, &light_positions[0]);
	//glTexImage2D(GL_TEXTURE_2D, 0, 0, 5, 1, 0, GL_RGBA, GL_UNSIGNED_BYTE, &light_positions[0]);
	glBindTexture(GL_TEXTURE_2D, 0);

	return;
}


///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Framebuffers
///////////////////////////////////////////////////////////////////////////////

struct FboInfo;
std::vector<FboInfo> fboList;

struct FboInfo {
	GLuint framebufferId;
	GLuint colorTextureTarget;
	GLuint depthBuffer;
	GLuint normalTextureTarget;
	GLuint reflectiveTextureTarget;
	GLuint csPositionBuffer;
	GLuint metalFresnelShininessBuffer;
	GLuint emissionBuffer;
	GLuint colorBuffer;
	GLuint skyboxColor;
	int width;
	int height;
	bool isComplete;

	FboInfo(int w, int h) {
		isComplete = false;
		width = w;
		height = h;
		// Generate two textures and set filter parameters (no storage allocated yet)
		glGenTextures(1, &colorTextureTarget);
		glBindTexture(GL_TEXTURE_2D, colorTextureTarget);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glGenTextures(1, &depthBuffer);
		glBindTexture(GL_TEXTURE_2D, depthBuffer);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glGenTextures(1, &normalTextureTarget);
		glBindTexture(GL_TEXTURE_2D, normalTextureTarget);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glGenTextures(1, &reflectiveTextureTarget);
		glBindTexture(GL_TEXTURE_2D, reflectiveTextureTarget);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glGenTextures(1, &csPositionBuffer);
		glBindTexture(GL_TEXTURE_2D, csPositionBuffer);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);


		glGenTextures(1, &metalFresnelShininessBuffer);
		glBindTexture(GL_TEXTURE_2D, metalFresnelShininessBuffer);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		/*
		glGenTextures(1, &fresnelBuffer);
		glBindTexture(GL_TEXTURE_2D, fresnelBuffer);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glGenTextures(1, &shininessBuffer);
		glBindTexture(GL_TEXTURE_2D, shininessBuffer);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		*/
		glGenTextures(1, &emissionBuffer);
		glBindTexture(GL_TEXTURE_2D, emissionBuffer);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glGenTextures(1, &skyboxColor);
		glBindTexture(GL_TEXTURE_2D, skyboxColor);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);


		// allocate storage for textures
		resize(width, height);

		///////////////////////////////////////////////////////////////////////
		// Generate and bind framebuffer
		///////////////////////////////////////////////////////////////////////
		// >>> @task 1
		// check if framebuffer is complete
		glGenFramebuffers(1, &framebufferId);
		glBindFramebuffer(GL_FRAMEBUFFER, framebufferId);

		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, colorTextureTarget, 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, normalTextureTarget, 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, reflectiveTextureTarget, 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, GL_TEXTURE_2D, csPositionBuffer, 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT4, GL_TEXTURE_2D, metalFresnelShininessBuffer, 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT5, GL_TEXTURE_2D, emissionBuffer, 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT6, GL_TEXTURE_2D, skyboxColor, 0);
		unsigned int buffers[7] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3, 
									GL_COLOR_ATTACHMENT4, GL_COLOR_ATTACHMENT5, GL_COLOR_ATTACHMENT6 };
		glDrawBuffers(7, buffers);

		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthBuffer, 0);

		

		isComplete = checkFramebufferComplete();

		// bind default framebuffer, just in case.
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	// if no resolution provided
	FboInfo() : isComplete(false)
		, framebufferId(UINT32_MAX)
		, colorTextureTarget(UINT32_MAX)
		, depthBuffer(UINT32_MAX)
		, width(0)
		, height(0)
	{};

	void resize(int w, int h) {
		width = w;
		height = h;
		// Allocate a texture
		glBindTexture(GL_TEXTURE_2D, colorTextureTarget);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
		float borderColor[] = { 0.0f, 0.0f, 0.0f, 0.0f };
		glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);

		glBindTexture(GL_TEXTURE_2D, normalTextureTarget);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);


		glBindTexture(GL_TEXTURE_2D, reflectiveTextureTarget);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);

		glBindTexture(GL_TEXTURE_2D, csPositionBuffer);
		// Make sure that if we sample outside the range of the texture size, then we will simply get 0.
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
		glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
		
		glBindTexture(GL_TEXTURE_2D, metalFresnelShininessBuffer);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
		/*
		glBindTexture(GL_TEXTURE_2D, fresnelBuffer);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);

		glBindTexture(GL_TEXTURE_2D, shininessBuffer);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
		*/
		glBindTexture(GL_TEXTURE_2D, emissionBuffer);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);

		glBindTexture(GL_TEXTURE_2D, skyboxColor);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);

		// generate a depth texture
		glBindTexture(GL_TEXTURE_2D, depthBuffer);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
	}

	bool checkFramebufferComplete(void) {
		// Check that our FBO is correctly set up, this can fail if we have
		// incompatible formats in a buffer, or for example if we specify an
		// invalid drawbuffer, among things.
		glBindFramebuffer(GL_FRAMEBUFFER, framebufferId);
		GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
		if (status != GL_FRAMEBUFFER_COMPLETE) {
			labhelper::fatal_error("Framebuffer not complete");
		}
		
		return (status == GL_FRAMEBUFFER_COMPLETE);
	}
};

///////////////////////////////////////////////////////////////////////////////
// The load shaders function is called once from initialize() and then 
// whenever you press the Reload Shaders button
///////////////////////////////////////////////////////////////////////////////
void loadShaders(bool is_reload)
{
	backgroundProgram = labhelper::loadShaderProgram("../projectmap/shaders/background.vert", "../projectmap/shaders/background.frag", is_reload);
	shaderProgram     = labhelper::loadShaderProgram("../projectmap/shaders/simple.vert",     "../projectmap/shaders/simple.frag", is_reload);
	postFxShader      = labhelper::loadShaderProgram("../projectmap/shaders/postFx.vert",     "../projectmap/shaders/postFx.frag", is_reload);
	SSAOShader        = labhelper::loadShaderProgram("../projectmap/shaders/postFx.vert", "../projectmap/shaders/SSAO.frag", is_reload);
	genGBufferShader = labhelper::loadShaderProgram("../projectmap/shaders/simple.vert", "../projectmap/shaders/genGBuffer.frag");
	visualizeTestGBufferShader = labhelper::loadShaderProgram("../projectmap/shaders/postFx.vert", "../projectmap/shaders/visualizeGBuffer.frag", is_reload);
	lightPassShader = labhelper::loadShaderProgram("../projectmap/shaders/lightpass.vert", "../projectmap/shaders/lightpass.frag", is_reload);
	blurShader = labhelper::loadShaderProgram("../projectmap/shaders/postFx.vert", "../projectmap/shaders/blur.frag", is_reload);
	part1shader = labhelper::loadShaderProgram("../projectmap/shaders/particle.vert", "../projectmap/shaders/particle.frag", is_reload);
	simpleShader = labhelper::loadShaderProgram("../projectmap/shaders/simple2.vert", "../projectmap/shaders/simple2.frag", is_reload);
	lightingTiledShader = labhelper::loadShaderProgram("../projectmap/shaders/lightpass.vert", "../projectmap/shaders/lightpassTiled.frag", is_reload);

	/*
	GLuint shader = labhelper::loadShaderProgram("../lab4-cubemapping/simple.vert", "../lab4-cubemapping/simple.frag", is_reload);
	if (shader != 0) shaderProgram = shader;
	shader = labhelper::loadShaderProgram("../lab4-cubemapping/background.vert", "../lab4-cubemapping/background.frag", is_reload);
	if (shader != 0) backgroundProgram = shader;*/
}
void initGL()
{
	// enable Z-buffering
	glEnable(GL_DEPTH_TEST);

	// enable backface culling
	glEnable(GL_CULL_FACE);

	// Load some models.
	landingpadModel = labhelper::loadModelFromOBJ("../scenes/landingpad.obj");
	cameraModel = labhelper::loadModelFromOBJ("../scenes/camera.obj");
	fighterModel = labhelper::loadModelFromOBJ("../scenes/cornell.obj");
	sphereModel = labhelper::loadModelFromOBJ("../scenes/sphere.obj");
	cornellModel = labhelper::loadModelFromOBJ("../scenes/cornell.obj");	
	planeModel = labhelper::loadModelFromOBJ("../scenes/plane.obj");
	//sponzaModel = labhelper::loadModelFromOBJ("../scenes/sponza.obj");


	loadShaders(false);
	/*
	// load and set up default shader
	backgroundProgram = labhelper::loadShaderProgram("../projectmap/shaders/background.vert", "../projectmap/shaders/background.frag");
	shaderProgram     = labhelper::loadShaderProgram("../projectmap/shaders/simple.vert",     "../projectmap/shaders/simple.frag");
	postFxShader      = labhelper::loadShaderProgram("../projectmap/shaders/postFx.vert",     "../projectmap/shaders/postFx.frag");
	SSAOShader        = labhelper::loadShaderProgram("../projectmap/shaders/postFx.vert", "../projectmap/shaders/SSAO.frag");
	genGBufferShader = labhelper::loadShaderProgram("../projectmap/shaders/simple.vert", "../projectmap/shaders/genGBuffer.frag");
	visualizeTestGBufferShader = labhelper::loadShaderProgram("../projectmap/shaders/postFx.vert", "../projectmap/shaders/visualizeGBuffer.frag");
	lightPassShader = labhelper::loadShaderProgram("../projectmap/shaders/lightpass.vert", "../projectmap/shaders/lightpass.frag");
	blurShader = labhelper::loadShaderProgram("../projectmap/shaders/postFx.vert", "../projectmap/shaders/blur.frag");
	part1shader = labhelper::loadShaderProgram("../projectmap/shaders/particle.vert", "../projectmap/shaders/particle.frag");
	simpleShader = labhelper::loadShaderProgram("../projectmap/shaders/simple2.vert", "../projectmap/shaders/simple2.frag");
	lightingTiledShader = labhelper::loadShaderProgram("../projectmap/shaders/lightpass.vert", "../projectmap/shaders/lightpassTiled.frag");*/
	///////////////////////////////////////////////////////////////////////////
	// Load environment map5
	///////////////////////////////////////////////////////////////////////////
	const int roughnesses = 8;
	std::vector<std::string> filenames;
	for (int i = 0; i < roughnesses; i++)
		filenames.push_back("../scenes/envmaps/" + envmap_base_name + "_dl_" + std::to_string(i) + ".hdr");

	reflectionMap = labhelper::loadHdrMipmapTexture(filenames);
	environmentMap = labhelper::loadHdrTexture("../scenes/envmaps/" + envmap_base_name + ".hdr");
	irradianceMap = labhelper::loadHdrTexture("../scenes/envmaps/" + envmap_base_name + "_irradiance.hdr");

	///////////////////////////////////////////////////////////////////////////
	// Setup Framebuffers
	///////////////////////////////////////////////////////////////////////////
	int w, h;
	SDL_GetWindowSize(g_window, &w, &h);
	const int numFbos = 5;
	for (int i = 0; i < numFbos; i++)
		fboList.push_back(FboInfo(w, h));

}

GLuint framebufferId;
void display()
{
	///////////////////////////////////////////////////////////////////////////
	// Check if any framebuffer needs to be resized
	///////////////////////////////////////////////////////////////////////////
	int w, h;
	SDL_GetWindowSize(g_window, &w, &h);
	for (int i = 0; i < fboList.size(); i++) {
		if (fboList[i].width != w || fboList[i].height != h)
			fboList[i].resize(w, h);
	}

	///////////////////////////////////////////////////////////////////////////
	// setup matrices
	///////////////////////////////////////////////////////////////////////////
	projectionMatrix = perspective(radians(55.0f), float(w) / float(h), 1.0f, 10000.0f);
	viewMatrix = lookAt(cameraPosition, cameraPosition + cameraDirection, worldUp);
	mouse_view_matrix = lookAt(securityCamDirection, securityCamPos + securityCamDirection, worldUp);
	///////////////////////////////////////////////////////////////////////////
	// Bind the environment map(s) to unused texture units
	///////////////////////////////////////////////////////////////////////////
	glActiveTexture(GL_TEXTURE6);
	glBindTexture(GL_TEXTURE_2D, environmentMap);
	glActiveTexture(GL_TEXTURE7);
	glBindTexture(GL_TEXTURE_2D, irradianceMap);
	glActiveTexture(GL_TEXTURE8);
	glBindTexture(GL_TEXTURE_2D, reflectionMap);

	
	///////////////////////////////////////////////////////////////////////////
	// draw scene from camera
	///////////////////////////////////////////////////////////////////////////
	FboInfo &gBuffer = fboList[0];
	FboInfo &skyboxFb = fboList[1];
	FboInfo &ssaoFB = fboList[2];
	FboInfo &lightpass = fboList[3];
	FboInfo &blurpass = fboList[4];

	///////////////////////////////////////////////////////////////////////////
	// Render to G-Buffer, (Position,Normal,Reflectivity,Fresnel, Metalic, etc..)
	///////////////////////////////////////////////////////////////////////////
	glBindFramebuffer(GL_FRAMEBUFFER, gBuffer.framebufferId);
	glViewport(0, 0, w, h);
	glClearColor(0.2, 0.2, 0.8, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(genGBufferShader);


	// Light source
	labhelper::setUniformSlow(genGBufferShader, "environment_multiplier", environment_multiplier);
	labhelper::setUniformSlow(genGBufferShader, "inv_PV", inverse(projectionMatrix * viewMatrix));
	labhelper::setUniformSlow(genGBufferShader, "camera_pos", cameraPosition);
	vec4 viewSpaceLightPosition = viewMatrix * vec4(lightPosition, 1.0f);
	labhelper::setUniformSlow(genGBufferShader, "viewSpaceLightPosition", vec3(viewSpaceLightPosition));
	// camera
	labhelper::setUniformSlow(genGBufferShader, "viewInverse", inverse(viewMatrix));
	
	// landing pad 
	mat4 modelMatrix(1.0f);
	labhelper::setUniformSlow(genGBufferShader, "modelViewProjectionMatrix", projectionMatrix * viewMatrix * modelMatrix);
	labhelper::setUniformSlow(genGBufferShader, "modelViewMatrix", viewMatrix * modelMatrix);
	labhelper::setUniformSlow(genGBufferShader, "normalMatrix", inverse(transpose(viewMatrix * modelMatrix)));
	//labhelper::render(landingpadModel);

	mat4 fighterModelMatrix = translate(15.0f * worldUp);// *rotate(currentTime * -float(M_PI) / 4.0f, worldUp);
	labhelper::setUniformSlow(genGBufferShader, "modelViewProjectionMatrix", projectionMatrix * viewMatrix * fighterModelMatrix);
	labhelper::setUniformSlow(genGBufferShader, "modelViewMatrix", viewMatrix * fighterModelMatrix);
	labhelper::setUniformSlow(genGBufferShader, "normalMatrix", inverse(transpose(viewMatrix * fighterModelMatrix)));
	//labhelper::render(fighterModel);

	
	mat4 sphereModelMatrix = scale(vec3(50.0f, 50.0f, 50.0f))* translate(1.0f*worldUp);// = translate(15.0f * worldUp) * rotate(currentTime * -float(M_PI) / 4.0f, worldUp);
	labhelper::setUniformSlow(genGBufferShader, "modelViewProjectionMatrix", projectionMatrix * viewMatrix* sphereModelMatrix);
	labhelper::setUniformSlow(genGBufferShader, "modelViewMatrix", viewMatrix * sphereModelMatrix);
	labhelper::setUniformSlow(genGBufferShader, "normalMatrix", inverse(transpose(viewMatrix * sphereModelMatrix)));

	labhelper::render(cornellModel);
	vec3(150.0f, 50.0f, 150.0f);
	mat4 planeModelMatrix = scale(vec3(1500.0f, 50.0f, 2000.0f))* translate(0.97f*worldUp);
	labhelper::setUniformSlow(genGBufferShader, "modelViewProjectionMatrix", projectionMatrix * viewMatrix* planeModelMatrix);
	labhelper::setUniformSlow(genGBufferShader, "modelViewMatrix", viewMatrix * planeModelMatrix);
	labhelper::setUniformSlow(genGBufferShader, "normalMatrix", inverse(transpose(viewMatrix * planeModelMatrix)));
	labhelper::render(planeModel);

	/*
	mat4 sponzaModelMatrix = scale(vec3(1.0f, 1.0f, 1.0f));
	labhelper::setUniformSlow(genGBufferShader, "modelViewProjectionMatrix", projectionMatrix * viewMatrix* sponzaModelMatrix);
	labhelper::setUniformSlow(genGBufferShader, "modelViewMatrix", viewMatrix * sponzaModelMatrix);
	labhelper::setUniformSlow(genGBufferShader, "normalMatrix", inverse(transpose(viewMatrix * sponzaModelMatrix)));
	labhelper::render(sponzaModel);
	*/
	///////////////////////////////////////////////////////////////////////////
	// SSAO pass.
	///////////////////////////////////////////////////////////////////////////
	
	glBindFramebuffer(GL_FRAMEBUFFER, ssaoFB.framebufferId);
	glViewport(0, 0, w, h);
	glClearColor(0.2, 0.2, 0.8, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(SSAOShader);

	
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, rotationTexture);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, gBuffer.depthBuffer);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, gBuffer.normalTextureTarget);

	labhelper::setUniformSlow(SSAOShader, "inverseProjMatrix", inverse(projectionMatrix));
	labhelper::setUniformSlow(SSAOShader, "projectionMatrix", projectionMatrix);
	glUniform3fv(glGetUniformLocation(SSAOShader, "hemisphereSamples"), 16,
		glm::value_ptr(hemiSphereSamples[0]));


	labhelper::drawFullScreenQuad();
	
	///////////////////////////////////////////////////////////////////////////
	// Blur pass
	///////////////////////////////////////////////////////////////////////////
	glBindFramebuffer(GL_FRAMEBUFFER, blurpass.framebufferId);
	glViewport(0, 0, w, h);
	glClearColor(0.2, 0.2, 0.8, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(blurShader);


	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, ssaoFB.colorTextureTarget);
	labhelper::setUniformSlow(blurShader, "filterSize", 0);
	labhelper::drawFullScreenQuad();
	///////////////////////////////////////////////////////////////////////////
	// Skybox pass
	///////////////////////////////////////////////////////////////////////////
	
	glBindFramebuffer(GL_FRAMEBUFFER, skyboxFb.framebufferId);
	glViewport(0, 0, w, h);
	glClearColor(0.2, 0.2, 0.8, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(backgroundProgram);
	labhelper::setUniformSlow(backgroundProgram, "environment_multiplier", environment_multiplier);
	labhelper::setUniformSlow(backgroundProgram, "inv_PV", inverse(projectionMatrix * viewMatrix));
	labhelper::setUniformSlow(backgroundProgram, "camera_pos", cameraPosition);
	labhelper::drawFullScreenQuad();
	
	///////////////////////////////////////////////////////////////////////////
	// Particle positions pass, update logic of particles and send them to light pass.
	//////////////////////////////////////////////////////////////////////////

	///////////////////// PARTICLE UPDATE ///////////////////////////
	
	particle_system.max_size = max_active_particles;

	for (int i = 0; i < 16; i++) {
		Particle p;
		const float theta = labhelper::uniform_randf(0.f, 2.f * M_PI);
		const float u = labhelper::uniform_randf(-1.f, 1.f);
		//const float u = labhelper::uniform_randf(0.96f, 1.f);
		vec3 g = glm::vec3(u, sqrt(1.f - u * u) * cosf(theta), sqrt(1.f - u * u) * sinf(theta))*30.0f;
		p.lifetime = 0.0f;
		p.life_length = 0.5f;
		//p.pos = vec3(labhelper::randf()*100.0f, labhelper::randf()*50.0f, labhelper::randf()*250.0f);
		p.pos = vec3(0.0f);
		p.velocity = g;

		particle_system.spawn(p);
	}
	particle_system.process_particles(.0005f, collisionBox, vel);

	///////////////////// PARTICLE UPLOAD //////////////////////////////////
	active_particles = particle_system.particles.size();
	auto start3 = std::chrono::system_clock::now();
	int NR_OF_TILES = (int)(w / TILE_WIDTH) * (int)(h / TILE_HEIGHT) + (int)(w / TILE_WIDTH);
	if (useTiled) {
		

		for (Tile t : tiles) {

			t.lights.clear();
		}
		tiles.clear();

		tiles.resize(NR_OF_TILES);

		light_offset.clear();
		light_offset.resize(NR_OF_TILES);

		light_size.clear();
		light_size.resize(NR_OF_TILES);

		light_list.clear();
	}
	auto end3 = std::chrono::system_clock::now();
	std::chrono::duration<double> diff = end3 - start3;
	tileClearingTime = diff.count()*1000.0;
	
	auto start2 = std::chrono::system_clock::now();
	for (int i = 0; i < active_particles; i++) {
		particlePositions[i] = particle_system.particles[i].pos + vec3(0.0f, 30.0f, 0.0f);
		//particleColors[i] = vec3(1.0f, 1.0f, 1.0f);
		//particleColors[i] = vec3(labhelper::randf(), labhelper::randf(), labhelper::randf());
		data2[i] = vec4(particlePositions[i], particle_system.particles[i].lifetime);
		if (useTiled) {
			shader_dat[i] = vec4(particlePositions[i], 1.0f);

			tileClassification(i, tiles, w, h, (int)(w / TILE_WIDTH), (int)(h / TILE_HEIGHT), TILE_WIDTH, TILE_HEIGHT, viewMatrix* vec4(particlePositions[i], 1.0f), point_light_radius, -1.0f, projectionMatrix);
		}
		// Just some debug stuff I did to see viewspace and screen space pos of light.
		/*vec4 pos = viewMatrix * vec4(particlePositions[i], 1.0f);
		pos = projectionMatrix * pos;
		pos /= pos.w*/
		//std::cout << particlePositions[i].x << ", " << particlePositions[i].y << ", " << particlePositions[i].z << std::endl;
		//std::cout << pos.x << ", " << pos.y << ", " << pos.z << std::endl;
	}
	
	auto end2 = std::chrono::system_clock::now();
	std::chrono::duration<double> diff3 = end2 - start2;
	tileClassTime = diff3.count()*1000.0;

	std::sort(data2.begin(), std::next(data2.begin(), active_particles),
		[](const vec4 &lhs, const vec4 &rhs) { return lhs.z < rhs.z; });
	if (useTiled) {
		create_tile_lists(tiles, light_offset, light_size, light_list);
	}
	
	///////////////////////////////////////////////////////////////////////////
	// Light Pass, render an image with lighting to a texture.
	//////////////////////////////////////////////////////////////////////////
	GLuint lighting_shader = lightPassShader;
	if (useTiled)
		lighting_shader = lightingTiledShader;
	
	glBindFramebuffer(GL_FRAMEBUFFER, lightpass.framebufferId);
	glViewport(0, 0, w, h);
	glClearColor(0.9, 0.2, 0.8, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(lighting_shader);
	
	float ap = active_particles;
	labhelper::setUniformSlow(lighting_shader, "active_particles", ap);
	

	glUniform3fv(glGetUniformLocation(lighting_shader, "lightColors"), 50,
		glm::value_ptr(lightColors[0]));

	
	
	auto start = std::chrono::system_clock::now();
	if (useTiled) {
		///////////////////// TILE DATA UPLOAD //////////////////////////////////

		labhelper::setUniformSlow(lightingTiledShader, "TILE_WIDTH", TILE_WIDTH);
		labhelper::setUniformSlow(lightingTiledShader, "TILE_HEIGHT", TILE_WIDTH);
		labhelper::setUniformSlow(lightingTiledShader, "nr_of_tiles", NR_OF_TILES);

		labhelper::setUniformSlow(lightingTiledShader, "width", (int)w);
		labhelper::setUniformSlow(lightingTiledShader, "height", (int)h);

		glBindBuffer(GL_SHADER_STORAGE_BUFFER, ssbo_tile_rendering);
		glBufferSubData(GL_SHADER_STORAGE_BUFFER, 0, sizeof(vec4) * nr_of_lights, &shader_dat[0]);

		glBufferSubData(GL_SHADER_STORAGE_BUFFER, sizeof(vec4) * nr_of_lights, sizeof(int) * nr_of_tiles, &light_size[0]);
		glBufferSubData(GL_SHADER_STORAGE_BUFFER, sizeof(vec4) * nr_of_lights + sizeof(int) * nr_of_tiles, sizeof(int) * nr_of_tiles, &light_offset[0]);
		if (!light_list.empty())
			glBufferSubData(GL_SHADER_STORAGE_BUFFER, (sizeof(vec4) * nr_of_lights) + (sizeof(int) * nr_of_tiles * 2), sizeof(int)*light_list.size(), &light_list[0]);

		glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
	}
	else {
		glUniform3fv(glGetUniformLocation(lighting_shader, "particleLights"), active_particles,
			glm::value_ptr(particlePositions[0]));
	}


	auto end = std::chrono::system_clock::now();
	diff = end - start;

	tileUploadTime = diff.count()*1000.0;


	///////////////////// ////////////////// //////////////////////////////////
	//update_light_positions_texture(particlePositions, active_particles);
	unsigned int queryID2[2];

	// generate two queries
	glGenQueries(2, queryID2);
	glQueryCounter(queryID2[0], GL_TIMESTAMP);
	labhelper::setUniformSlow(lighting_shader, "MVP", projectionMatrix * viewMatrix);
	labhelper::setUniformSlow(lighting_shader, "viewSpaceLightPosition", vec3(viewSpaceLightPosition));
	labhelper::setUniformSlow(lighting_shader, "environment_multiplier", environment_multiplier);
	labhelper::setUniformSlow(lighting_shader, "viewInverse", inverse(viewMatrix));
	labhelper::setUniformSlow(lighting_shader, "point_light_color", point_light_color);
	labhelper::setUniformSlow(lighting_shader, "point_light_intensity_multiplier", point_light_intensity_multiplier);
	labhelper::setUniformSlow(lighting_shader, "viewMatrix", viewMatrix);
	labhelper::setUniformSlow(lighting_shader, "inverseProjMatrix", inverse(projectionMatrix));
	labhelper::setUniformSlow(lighting_shader, "radius", point_light_radius);

	// Bind the texture we want to use in the postprocessing.
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, gBuffer.colorTextureTarget);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, gBuffer.depthBuffer);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, gBuffer.normalTextureTarget);
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, gBuffer.reflectiveTextureTarget);
	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, gBuffer.csPositionBuffer);
	glActiveTexture(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_2D, gBuffer.metalFresnelShininessBuffer);
	glActiveTexture(GL_TEXTURE6);
	glBindTexture(GL_TEXTURE_2D, light_position_texture);
	//glActiveTexture(GL_TEXTURE6);
	//glBindTexture(GL_TEXTURE_2D, gBuffer.emissionBuffer);
	glActiveTexture(GL_TEXTURE7);
	glBindTexture(GL_TEXTURE_2D, skyboxFb.colorTextureTarget);
	glActiveTexture(GL_TEXTURE8);
	glBindTexture(GL_TEXTURE_2D, environmentMap);
	glActiveTexture(GL_TEXTURE9);
	glBindTexture(GL_TEXTURE_2D, irradianceMap);
	glActiveTexture(GL_TEXTURE10);
	glBindTexture(GL_TEXTURE_2D, reflectionMap);
	glActiveTexture(GL_TEXTURE11);
	glBindTexture(GL_TEXTURE_2D, blurpass.colorTextureTarget);
	glActiveTexture(GL_TEXTURE12);
	glBindTexture(GL_TEXTURE_2D, light_color_texture);

	labhelper::drawFullScreenQuad();
	glQueryCounter(queryID2[1], GL_TIMESTAMP);
	GLint stopTimerAvailable = 0;
	while (!stopTimerAvailable) {
		glGetQueryObjectiv(queryID2[1],
			GL_QUERY_RESULT_AVAILABLE,
			&stopTimerAvailable);
	}
	// get query results
	glGetQueryObjectui64v(queryID2[0], GL_QUERY_RESULT, &startTime);
	glGetQueryObjectui64v(queryID2[1], GL_QUERY_RESULT, &stopTime);
	lightpassTime = (stopTime - startTime) / 1000000.0;
	///////////////////////////////////////////////////////////////////////////
	// SSR pass.
	///////////////////////////////////////////////////////////////////////////

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, w, h);
	glClearColor(0.2, 0.2, 0.8, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(postFxShader);

	vec4 viewSpaceCameraPosition = viewMatrix * vec4(cameraPosition, 1.0f);
	labhelper::setUniformSlow(postFxShader, "viewSpaceCameraPosition", vec3(viewSpaceCameraPosition));

	
	unsigned int queryID[2];

	// generate two queries
	glGenQueries(2, queryID);
	glQueryCounter(queryID[0], GL_TIMESTAMP);
	glActiveTexture(GL_TEXTURE0);
	// Bind the texture we want to use in the postprocessing.
	if(showSSAO)
		glBindTexture(GL_TEXTURE_2D, blurpass.colorTextureTarget);
	else
		glBindTexture(GL_TEXTURE_2D, lightpass.colorTextureTarget);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, gBuffer.depthBuffer);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, gBuffer.normalTextureTarget);
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, gBuffer.reflectiveTextureTarget);
	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, gBuffer.csPositionBuffer);
	labhelper::setUniformSlow(postFxShader, "inverseProjMatrix", inverse(projectionMatrix));
	labhelper::setUniformSlow(postFxShader, "projectionMatrix", projectionMatrix);
	labhelper::setUniformSlow(postFxShader, "time", currentTime);
	labhelper::setUniformSlow(postFxShader, "currentEffect", currentEffect);
	labhelper::setUniformSlow(postFxShader, "filterSize", filterSizes[filterSize - 1]);
	labhelper::setUniformSlow(postFxShader, "width", w);
	labhelper::setUniformSlow(postFxShader, "height", h);

	// Set basic_ssr_variables
	labhelper::setUniformSlow(postFxShader, "basic_stepLength", basic_ssr_trace_length);
	labhelper::setUniformSlow(postFxShader, "basic_maxBinarySteps", basic_ssr_binary_steps);
	labhelper::setUniformSlow(postFxShader, "basic_binarySearch", basic_ssr_binary_search);
	labhelper::setUniformSlow(postFxShader, "basic_maxDistance", basic_ssr_trace_steps);
	
	glUniform3fv(glGetUniformLocation(postFxShader, "hemisphereSamples"), 16,
		glm::value_ptr(hemiSphereSamples[0]));
	labhelper::drawFullScreenQuad();

	glQueryCounter(queryID[1], GL_TIMESTAMP);
	stopTimerAvailable = 0;
	while (!stopTimerAvailable) {
		glGetQueryObjectiv(queryID[1],
			GL_QUERY_RESULT_AVAILABLE,
			&stopTimerAvailable);
	}
	// get query results
	glGetQueryObjectui64v(queryID[0], GL_QUERY_RESULT, &startTime);
	glGetQueryObjectui64v(queryID[1], GL_QUERY_RESULT, &stopTime);
	ssrPassTime = (stopTime - startTime) / 1000000.0;
	//printf("Time spent on the GPU: %f ms\n", (stopTime - startTime) / 1000000.0);
	
	///////////////////////////////////////////////////////////////////////////
	// Forward rendering of particles
	///////////////////////////////////////////////////////////////////////////

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, w, h);
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vec4) * active_particles, &data2[0]);

	glUseProgram(part1shader);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, particleTexture);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, gBuffer.depthBuffer);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, lightpass.colorTextureTarget);

	glm::mat4 mvp = projectionMatrix * viewMatrix;
	GLuint MatrixID = glGetUniformLocation(part1shader, "MVP");
	glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &mvp[0][0]);

	labhelper::setUniformSlow(part1shader, "w", (float)w);
	labhelper::setUniformSlow(part1shader, "h", (float)h);
	labhelper::setUniformSlow(part1shader, "inverseView", inverse(viewMatrix));
	labhelper::setUniformSlow(part1shader, "inverseProjMatrix", inverse(projectionMatrix));
	labhelper::setUniformSlow(part1shader, "projectionMatrix", (projectionMatrix));
	labhelper::setUniformSlow(part1shader, "V", viewMatrix);
	labhelper::setUniformSlow(part1shader, "screen_x", (float)w);
	labhelper::setUniformSlow(part1shader, "screen_y", (float)h);
	labhelper::setUniformSlow(part1shader, "P", projectionMatrix);

	glBindVertexArray(VAO);
	glDrawArrays(GL_POINTS, 0, active_particles);
	glDisable(GL_BLEND);
	glEnable(GL_DEPTH_TEST);

	glUseProgram( 0 );

	CHECK_GL_ERROR();
}

bool handleEvents(void)
{
	// check events (keyboard among other)
	SDL_Event event;
	bool quitEvent = false;
	while (SDL_PollEvent(&event)) {
		if (event.type == SDL_QUIT || (event.type == SDL_KEYUP && event.key.keysym.sym == SDLK_ESCAPE)) {
			quitEvent = true;
		}
		if (event.type == SDL_MOUSEMOTION && !ImGui::IsMouseHoveringAnyWindow()) {
			// More info at https://wiki.libsdl.org/SDL_MouseMotionEvent
			static int prev_xcoord = event.motion.x;
			static int prev_ycoord = event.motion.y;
			int delta_x = event.motion.x - prev_xcoord;
			int delta_y = event.motion.y - prev_ycoord;

			if (event.button.button & SDL_BUTTON(SDL_BUTTON_LEFT)) {
				float rotationSpeed = 0.005f;
				mat4 yaw = rotate(rotationSpeed * -delta_x, worldUp);
				mat4 pitch = rotate(rotationSpeed * -delta_y, normalize(cross(cameraDirection, worldUp)));
				cameraDirection = vec3(pitch * yaw * vec4(cameraDirection, 0.0f));
				attraction_point = viewMatrix * vec4(cameraDirection.x, cameraDirection.y, 0.0f, 1.0f);
				attraction_point = -vec3(attraction_point.x, attraction_point.y, 0.0f);
				//std::cout << "New point set, " << attraction_point.x << "," << attraction_point.y << "\n";
			}

			if (event.button.button & SDL_BUTTON(SDL_BUTTON_RIGHT)) {
				float rotationSpeed = 0.005f;
				mat4 yaw = rotate(rotationSpeed * -delta_x, worldUp);
				mat4 pitch = rotate(rotationSpeed * -delta_y, normalize(cross(securityCamDirection, worldUp)));
				securityCamDirection = vec3(pitch * yaw * vec4(securityCamDirection, 0.0f));
				int w, h;
				SDL_GetWindowSize(g_window, &w, &h);
				float x = ((event.motion.x / (float)w) *2.0) - 1.0;
				float y = ((event.motion.y / (float)h) *2.0) - 1.0;

				vec4 ray_clip = vec4(x, -y, -1.0f, 1.0f);

				vec4 ray_eye = inverse(projectionMatrix) * ray_clip;

				ray_eye = vec4(ray_eye.x, ray_eye.y, -1.0f, 0.0f);

				vec4 ray_world = (inverse(viewMatrix))* ray_eye;
				ray_world = normalize(ray_world);

				vec3 ray_origin = cameraPosition;
				vec3 ray_direction = ray_world;

				vec3 plane_normal = vec3(0.0f, 0.0f, 1.0f);
				vec3 plane = vec3(0.0f, 0.0f, 0.0f);
				vec3 k = cameraPosition.z*ray_direction;
				k = vec3(k.x, k.y, 0.0f);
				attraction_point = k;
				//std::cout << "New point set, " <<  k.x << "," << k.y << "\n";
				

			}

			prev_xcoord = event.motion.x;
			prev_ycoord = event.motion.y;
		}
		if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_LCTRL) {
			if(event.type == SDL_MOUSEBUTTONDOWN)
				std::cout << "testing";

		}
	}

	// check keyboard state (which keys are still pressed)
	const uint8_t *state = SDL_GetKeyboardState(nullptr);
	int x, y;
	const uint32_t mouse_state = SDL_GetMouseState(&x, &y);
	vec3 cameraRight = cross(cameraDirection, worldUp);
	
	if (state[SDL_SCANCODE_W]) {
		cameraPosition += cameraSpeed* cameraDirection;
	}
	if (state[SDL_SCANCODE_S]) {
		cameraPosition -= cameraSpeed * cameraDirection;
	}
	if (state[SDL_SCANCODE_A]) {
		cameraPosition -= cameraSpeed * cameraRight;
	}
	if (state[SDL_SCANCODE_D]) {
		cameraPosition += cameraSpeed * cameraRight;
	}
	if (state[SDL_SCANCODE_Q]) {
		cameraPosition -= cameraSpeed * worldUp;
	}
	if (state[SDL_SCANCODE_E]) {
		cameraPosition += cameraSpeed * worldUp;
	}
	if (state[SDL_SCANCODE_LCTRL]) {
		repel = 1;
		std::cout << "repel on" << "\n";
	}
	if (state[SDL_SCANCODE_LALT]) {
		repel = 0;
		std::cout << "repel off" << "\n";
	}
	if (state[SDL_SCANCODE_X]) {
		stop_particles = 1;
		std::cout << "particles off" << "\n";
	}
	if (state[SDL_SCANCODE_C]) {
		stop_particles = 0;
		std::cout << "particles off" << "\n";
	}
	return quitEvent;
}

void gui()
{
	// Inform imgui of new frame
	ImGui_ImplSdlGL3_NewFrame(g_window);

	
	
	// ----------------- Set variables --------------------------
	ImGui::Text("Mode");
	ImGui::RadioButton("Basic SSR", &currentEffect, PostProcessingEffect::None);
	ImGui::RadioButton("Efficient SSR", &currentEffect, PostProcessingEffect::Sepia);
	ImGui::RadioButton("SSR OFF", &currentEffect, PostProcessingEffect::Bloom);
	ImGui::RadioButton("Depth", &currentEffect, PostProcessingEffect::Mushroom);
	ImGui::RadioButton("Reflective", &currentEffect, PostProcessingEffect::Grayscale);
	ImGui::RadioButton("Normals", &currentEffect, PostProcessingEffect::Blur);
	ImGui::RadioButton("compute_ray_test_on", &ray_test_program_on, 1);
	ImGui::SameLine();
	ImGui::RadioButton("compute_ray_test_off", &ray_test_program_on, 0);
	ImGui::RadioButton("compute_particle_test_on", &particle_simulation_on, 1);
	ImGui::SameLine();
	ImGui::RadioButton("compute_particle_test_off", &particle_simulation_on, 0);
	/*
	ImGui::RadioButton("Mushroom", &currentEffect, PostProcessingEffect::Mushroom);
	ImGui::RadioButton("Blur", &currentEffect, PostProcessingEffect::Blur);
	ImGui::SameLine();
	ImGui::SliderInt("Filter size", &filterSize, 1, 12);
	ImGui::RadioButton("Grayscale", &currentEffect, PostProcessingEffect::Grayscale);
	ImGui::RadioButton("All of the above", &currentEffect, PostProcessingEffect::Composition);
	ImGui::RadioButton("Mosaic", &currentEffect, PostProcessingEffect::Mosaic);
	ImGui::RadioButton("Separable Blur", &currentEffect, PostProcessingEffect::Separable_blur);
	ImGui::RadioButton("Bloom", &currentEffect, PostProcessingEffect::Bloom);*/


	ImGui::Text("Show SSAO");
	ImGui::SameLine();
	ImGui::RadioButton("ON", &showSSAO, 1);
	ImGui::SameLine();
	ImGui::RadioButton(" OFF", &showSSAO, 0);

	ImGui::Text("%.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
	ImGui::Text("SSR Pass: %.3f ms", ssrPassTime);
	ImGui::Text("Light pass: %.3f ms", lightpassTime);
	ImGui::Text("Tile upload: %.3f ms", tileUploadTime);
	ImGui::Text("Tile Classification: %.3f ms", tileClassTime);
	ImGui::Text("Tile Clearing: %.3f ms", tileClearingTime);
	ImGui::SliderInt("# of particles", &max_active_particles, 1, nr_of_lights);

	ImGui::Text("Tiled Deferred Shading");
	ImGui::SliderInt("", &useTiled, 0, 1);
	ImGui::SliderFloat("Point light radius", &point_light_radius, 15.0f, 150.0f);
	
	ImGui::Text("Basic screen space variables");
	ImGui::SliderInt("Max binary steps", &basic_ssr_binary_steps, 1, 10);
	ImGui::SliderInt("Max trace steps", &basic_ssr_trace_steps, 1, 1000);
	ImGui::SliderInt("Bin Search onoff", &basic_ssr_binary_search, 0, 1);
	ImGui::SliderFloat("Trace size", &basic_ssr_trace_length, 0.1f, 3.0f);
	ImGui::Text("Point light intensity");
	ImGui::SliderFloat("1", &point_light_intensity_multiplier, 1, 25000);

	ImGui::RadioButton("Box_Collision", &collisionBox, 1);
	ImGui::SameLine();
	ImGui::RadioButton("Explosion", &collisionBox, 0);
	ImGui::RadioButton("vel", &vel, 1);
	ImGui::SameLine();
	ImGui::RadioButton("vel_off", &vel, 0);
	ImGui::Text("Camera position: %.1f, %.1f, %.1f", cameraPosition.x, cameraPosition.y, cameraPosition.z);
	if (ImGui::Button("Reload Shaders")) {
		loadShaders(true);
	}
	if (ImGui::Button("DunkaDebug")) {

		int index = 0;
		for (int i = 0; i < 60; i++) {
			std::cout << "Current index: " << index;
			std::cout << ", Offset: " << light_offset[i] << ", Size: " << light_size[i];
			std::cout << " => Lights in tile: ";
			int offset = light_offset[i];
			for (int j = 0; j < light_size[i]; j++) {
				std::cout << light_list[offset + j] << ", ";
			}
			index++;
			std::cout << "\n";
		}
	}
	// ----------------------------------------------------------

	/*
	///////////////////////////////////////////////////////////////////////////
	// Helpers for getting lists of materials and meshes into widgets
	///////////////////////////////////////////////////////////////////////////
	static auto mesh_getter = [](void * vec, int idx, const char ** text) {
		auto& vector = *static_cast<std::vector<labhelper::Mesh>*>(vec);
		if (idx < 0 || idx >= static_cast<int>(vector.size())) { return false; }
		*text = vector[idx].m_name.c_str();
		return true;
	};

	static auto material_getter = [](void * vec, int idx, const char ** text) {
		auto& vector = *static_cast<std::vector<labhelper::Material>*>(vec);
		if (idx < 0 || idx >= static_cast<int>(vector.size())) { return false; }
		*text = vector[idx].m_name.c_str();
		return true;
	};

	///////////////////////////////////////////////////////////////////////////
	// List all meshes in the model and show properties for the selected
	///////////////////////////////////////////////////////////////////////////
	static int mesh_index = 0;
	static int material_index = fighterModel->m_meshes[mesh_index].m_material_idx;

	if (ImGui::CollapsingHeader("Meshes", "meshes_ch", true, true))
	{
		if (ImGui::ListBox("Meshes", &mesh_index, mesh_getter,
			(void*)&fighterModel->m_meshes, fighterModel->m_meshes.size(), 8)) {
			material_index = fighterModel->m_meshes[mesh_index].m_material_idx;
		}

		labhelper::Mesh & mesh = fighterModel->m_meshes[mesh_index];
		char name[256];
		strcpy(name, mesh.m_name.c_str());
		if (ImGui::InputText("Mesh Name", name, 256)) { mesh.m_name = name; }
		labhelper::Material & selected_material = fighterModel->m_materials[material_index];
		if (ImGui::Combo("Material", &material_index, material_getter,
			(void *)&fighterModel->m_materials, fighterModel->m_materials.size())) {
			mesh.m_material_idx = material_index;
		}
	}

	///////////////////////////////////////////////////////////////////////////
	// List all materials in the model and show properties for the selected
	///////////////////////////////////////////////////////////////////////////
	if (ImGui::CollapsingHeader("Materials", "materials_ch", true, true))
	{
		ImGui::ListBox("Materials", &material_index, material_getter,
			(void*)&fighterModel->m_materials, fighterModel->m_materials.size(), 8);
		labhelper::Material & material = fighterModel->m_materials[material_index];
		char name[256];
		strcpy(name, material.m_name.c_str());
		if (ImGui::InputText("Material Name", name, 256)) { material.m_name = name; }
		ImGui::ColorEdit3("Color", &material.m_color.x);
		ImGui::SliderFloat("Reflectivity", &material.m_reflectivity, 0.0f, 1.0f);
		ImGui::SliderFloat("Metalness", &material.m_metalness, 0.0f, 1.0f);
		ImGui::SliderFloat("Fresnel", &material.m_fresnel, 0.0f, 1.0f);
		ImGui::SliderFloat("shininess", &material.m_shininess, 0.0f, 25000.0f);
		ImGui::SliderFloat("Emission", &material.m_emission, 0.0f, 10.0f);
	}

	///////////////////////////////////////////////////////////////////////////
	// Light and environment map
	///////////////////////////////////////////////////////////////////////////
	if (ImGui::CollapsingHeader("Light sources", "lights_ch", true, true))
	{
		ImGui::SliderFloat("Environment multiplier", &environment_multiplier, 0.0f, 10.0f);
		ImGui::ColorEdit3("Point light color", &point_light_color.x);
		ImGui::SliderFloat("Point light intensity multiplier", &point_light_intensity_multiplier, 0.0f, 10000.0f);
	}

	///////////////////////////////////////////////////////////////////////////
	// A button for saving your results
	///////////////////////////////////////////////////////////////////////////
	if (ImGui::Button("Save Materials")) {
		labhelper::saveModelToOBJ(fighterModel, model_filename);
	}
	*/
	///////////////////////////////////////////////////////////////////////////
	// A button for reloading the shaders
	///////////////////////////////////////////////////////////////////////////

	// Render the GUI
	ImGui::Render();
}



GLuint create_quad_vao() {
	GLuint vao = 0, vbo = 0;
	float verts[] = { -1.0f, -1.0f, 
		0.0f, 0.0f,
		-1.0f, 1.0f,
		0.0f, 1.0f,
		1.0f,	-1.0f,
		1.0f, 0.0f,
		1.0f,	1.0f,
		1.0f, 1.0f };
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, 16 * sizeof(float), verts, GL_STATIC_DRAW);
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glEnableVertexAttribArray(0);
	GLintptr stride = 4 * sizeof(float);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, stride, NULL);
	glEnableVertexAttribArray(1);
	GLintptr offset = 2 * sizeof(float);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, stride, (GLvoid *)offset);
	return vao;
}


GLuint create_quad_program() {
	
	GLuint program = labhelper::loadShaderProgram("../projectmap/shaders/quad_test.vert", "../projectmap/shaders/quad_test.frag", 0);
	return program;
}

int main(int argc, char *argv[])
{
	g_window = labhelper::init_window_SDL("Advanced CG Project");
	
	initGL();


	bool stopRendering = false;
	auto startTime = std::chrono::system_clock::now();
	
	///////////////////////////////////////////////////////////////////////////
	// Generate samples for SSAO and noise texture
	///////////////////////////////////////////////////////////////////////////
	rotationTexture = createRandomRotations();
	for (int i = 0; i < 16; i++)
		hemiSphereSamples[i] = labhelper::cosineSampleHemisphere() * labhelper::randf();

	//GLuint rotationTexture = createRandomRotations();
	for (int i = 0; i < 200; i++) {
		//lightPositions[i] = vec3(labhelper::randf()*100.0f - 25.0f, labhelper::randf()*30.0f + 40.0f, labhelper::randf()*-75.0f + 40.0f);
		lightColors[i] = vec3(labhelper::randf()*0.5, labhelper::randf()*0.5, labhelper::randf()*0.5);

	}

	///////////////////////////////////////////////////////////////////////////
	// Particle Init
	///////////////////////////////////////////////////////////////////////////
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);
	
	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, 10000*sizeof(vec4), nullptr, GL_STATIC_DRAW);
	
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	data2.resize(10000);
	glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);
		
	int we, he, comp;
	unsigned char* image = stbi_load("../scenes/aura_test_1_32_1.png", &we, &he, &comp, STBI_rgb_alpha);
	glGenTextures(1, &particleTexture);
	glBindTexture(GL_TEXTURE_2D, particleTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, we, he, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	free(image);

	///////////////////////////////////////////////////////////////////////////

	int w, h;
	SDL_GetWindowSize(g_window, &w, &h);
	projectionMatrix = perspective(radians(55.0f), float(w) / float(h), 1.0f, 10000.0f);
	viewMatrix = lookAt(cameraPosition, cameraPosition + cameraDirection, worldUp);


	light_color_texture = gen_light_colors(100);
	light_position_texture = gen_light_positions(particlePositions, 5);
	///////////////////////////////////////////////////////////////////////////
	
	///////////////////////////////////////////////////////////////////////////
	// Light positions.
	///////////////////////////////////////////////////////////////////////////

	GLint64 ok;
	//glGetInteger64v(GL_MAX_UNIFORM_BLOCK_SIZE, &ok);
	glGetInteger64v(GL_MAX_VERTEX_UNIFORM_COMPONENTS, &ok);
	std::cout << ok << "\n";
	GLuint idx_;
	if (useTiled) {
		glGenBuffers(1, &ssbo_tile_rendering);
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER,12, ssbo_tile_rendering);

		// STREAM_DRAW or DYNAMIC_DRAW?

		glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(vec4) * 10000 + sizeof(int) * 3680 * 2 + sizeof(int) * 3680 * 10001, nullptr, GL_DYNAMIC_DRAW);
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, ssbo_tile_rendering);
		idx_ = glGetProgramResourceIndex(lightingTiledShader, GL_SHADER_STORAGE_BLOCK, "shader_data");
		//glShaderStorageBlockBinding(lightingTiledShader, idx_, 12);
	}

	///////////////////////////////////////////////////////////////////////////
	// Particle Compute shader Init (Hopefully computations for particles)
	///////////////////////////////////////////////////////////////////////////

	
	GLuint part_compute_shader = glCreateShader(GL_COMPUTE_SHADER);

	const std::string rayshader_ = "../projectmap/shaders/compute_part.glsl";
	std::ifstream cs_file(rayshader_);
	std::string cs_src((std::istreambuf_iterator<char>(cs_file)), std::istreambuf_iterator<char>());
	const char *cs = cs_src.c_str();

	glShaderSource(part_compute_shader, 1, &cs, NULL);
	glCompileShader(part_compute_shader);
	int success;
	char infoLog[512];
	glGetShaderiv(part_compute_shader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(part_compute_shader, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
	};

	part_compute_program = glCreateProgram();
	glAttachShader(part_compute_program, part_compute_shader);
	glLinkProgram(part_compute_program);
	glGetProgramiv(part_compute_program, GL_LINK_STATUS, &success);
	if (!success)
	{
		glGetProgramInfoLog(part_compute_program, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
	}

	/////////////////////// INITIALIZE THE PARTICLE BUFFERS IN COMPUTE SHADER FOR PARTICLE UPDATE //////////////////////////////
	int NUMBER_OF_PARTICLES = 1024 * 1024*2;
	std::vector<vec4> pps;
	pps.resize(NUMBER_OF_PARTICLES);
	std::vector<vec4> vcs;
	vcs.resize(NUMBER_OF_PARTICLES);


	for (int i = 0; i < NUMBER_OF_PARTICLES; i++) {
		const float theta = labhelper::uniform_randf(0.f, 2.f * M_PI);
		const float u = labhelper::uniform_randf(-1.f, 1.f);
		//const float u = labhelper::uniform_randf(0.96f, 1.f);

		vcs[i] = vec4(glm::vec3(u, sqrt(1.f - u * u) * cosf(theta), sqrt(1.f - u * u) * sinf(theta))*30.0f, 1.0f);

		pps[i] = vec4(labhelper::randf(), labhelper::randf(), labhelper::randf(), 1.0) * 50.0f;
	}

	vec3 partpos[3];
	partpos[0] = vec3(-5.5f, -0.5f, 0.0f);
	partpos[1] = vec3(0.5f, -0.5f, 0.0f);
	partpos[2] = vec3(0.0f, 0.5f, 0.0f);

	GLuint part_vao;
	glGenVertexArrays(1, &part_vao);
	glBindVertexArray(part_vao);

	glGenBuffers(1, &ssbo_particle_positions);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, ssbo_particle_positions);
	glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(vec4) * NUMBER_OF_PARTICLES, &pps[0], GL_STATIC_DRAW);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, ssbo_particle_positions);
	GLuint idx1 = glGetProgramResourceIndex(part_compute_program, GL_SHADER_STORAGE_BLOCK, "pos_particle_data");

	glBindBuffer(GL_ARRAY_BUFFER, ssbo_particle_positions);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	
	glGenBuffers(1, &ssbo_particle_velocities);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 4, ssbo_particle_velocities);

	glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(vec4) * NUMBER_OF_PARTICLES, &vcs[0], GL_STATIC_DRAW);
	GLuint idx2 = glGetProgramResourceIndex(part_compute_program, GL_SHADER_STORAGE_BLOCK, "vel_particle_data");

	
	



	//§glShaderStorageBlockBinding(part_compute_program, idx, 3);
	///////////////////////////////////////////////////////////////////////////
	// Ray Compute shader (Just testing some raytracing)
	///////////////////////////////////////////////////////////////////////////
	
	int work_grp_cnt[3];

	glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 0, &work_grp_cnt[0]);
	glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 1, &work_grp_cnt[1]);
	glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 2, &work_grp_cnt[2]);

	printf("max global (total) work group size x:%i y:%i z:%i\n",
		work_grp_cnt[0], work_grp_cnt[1], work_grp_cnt[2]);

	int work_grp_size[3];

	glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 0, &work_grp_size[0]);
	glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 1, &work_grp_size[1]);
	glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 2, &work_grp_size[2]);

	printf("max local (in one shader) work group sizes x:%i y:%i z:%i\n",
		work_grp_size[0], work_grp_size[1], work_grp_size[2]);

	int work_grp_inv;
	glGetIntegerv(GL_MAX_COMPUTE_WORK_GROUP_INVOCATIONS, &work_grp_inv);
	printf("max local work group invocations %i\n", work_grp_inv);

	int tex_w = 1280, tex_h = 720;
	glGenTextures(1, &tex_output);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, tex_output);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, tex_w, tex_h, 0, GL_RGBA, GL_FLOAT,NULL);
	//glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA32F, w, h);
	glBindImageTexture(0, tex_output, 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA32F);
	
	GLuint ray_shader = glCreateShader(GL_COMPUTE_SHADER);

	const std::string rayshader = "../projectmap/shaders/computeTest.glsl";
	std::ifstream cs_file_(rayshader);
	std::string cs_src_((std::istreambuf_iterator<char>(cs_file_)), std::istreambuf_iterator<char>());
	const char *cs_ = cs_src_.c_str();

	glShaderSource(ray_shader, 1, &cs_, NULL);
	glCompileShader(ray_shader);
	glGetShaderiv(ray_shader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(ray_shader, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
	};

	ray_program = glCreateProgram();
	glAttachShader(ray_program, ray_shader);
	glLinkProgram(ray_program);
	glGetProgramiv(ray_program, GL_LINK_STATUS, &success);
	if (!success)
	{
		glGetProgramInfoLog(ray_program, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
	}
	//GLuint quad_vao = create_quad_vao();
	GLuint quad_program = labhelper::loadShaderProgram("../projectmap/shaders/postFx.vert", "../projectmap/shaders/quad_test.frag");
	
	
	///////////////////////////////////////////////////////////////////////////

	float vertices[] = {
		// positions          // colors           // texture coords
		1.0f,  1.0f,     1.0f, 1.0f,   // top right
		1.0f, -1.0f,     1.0f, 0.0f,   // bottom right
		-1.0f, -1.0f,    0.0f, 0.0f,   // bottom left
		-1.0f,  1.0f,    0.0f, 1.0f    // top left 
	};
	unsigned int indices[] = {
		0, 1, 3, // first triangle
		1, 2, 3  // second triangle
	};
	unsigned int VBO, VAO, EBO;
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);

	glBindVertexArray(VAO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	// position attribute
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)(2 * sizeof(float)));
	glEnableVertexAttribArray(1);


	unsigned int texture;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	// set the texture wrapping/filtering options (on the currently bound texture object)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	// load and generate the texture
	int width1, height1, nrChannels;
	unsigned char *data = stbi_load("../scenes/bricks.jpg", &width1, &height1, &nrChannels, 0);
	if (data)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width1, height1, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		//glGenerateMipmap(GL_TEXTURE_2D);
	}
	else
	{
		std::cout << "Failed to load texture" << std::endl;
	}
	stbi_image_free(data);

	GLuint textTest = labhelper::loadShaderProgram("../projectmap/shaders/test_texture.vert", "../projectmap/shaders/test_texture.frag", 0);
	//SDL_GetWindowSize(g_window, &w, &h);
	//SDL_SetWindowSize(g_window, 1270, 591);

	
	float __vertices[] = {
		-0.5f, -0.5f, 0.0f,
		0.5f, -0.5f, 0.0f,
		0.0f,  0.5f, 0.0f
	};

	GLuint _vbo;
	GLuint _vao;

	glGenVertexArrays(1, &_vao);
	glBindVertexArray(_vao);

	glGenBuffers(1, &_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, _vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(__vertices), __vertices, GL_STATIC_DRAW);
	
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);
	
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	GLuint temp_program = labhelper::loadShaderProgram("../projectmap/shaders/part.vert", "../projectmap/shaders/part_test.frag", 0);
	while (!stopRendering) {
		//update currentTime
		int w, h;
		std::chrono::duration<float> timeSinceStart = std::chrono::system_clock::now() - startTime;
		currentTime = timeSinceStart.count();
		if (particle_simulation_on) {
			
			glUseProgram(part_compute_program);

			labhelper::setUniformSlow(part_compute_program, "stop_particles", stop_particles);
			labhelper::setUniformSlow(part_compute_program, "repel", repel);
			labhelper::setUniformSlow(part_compute_program, "gravity_center", vec3(0.0f, 0.0f, 0.0f));
			labhelper::setUniformSlow(part_compute_program, "attraction_point", attraction_point);
			//glDispatchComputeGroupSizeARB(
			//	num_groups,      1, 1,
			//	work_group_size, 1, 1);

			glDispatchCompute(((GLuint)(NUMBER_OF_PARTICLES)/1024) +1, (GLuint)1, 1);
			glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT | GL_VERTEX_ATTRIB_ARRAY_BARRIER_BIT);
			
			
			// TODO: Just render a couple of points in 3d space with camera movement, then extend updates with compute shader.allts
			SDL_GetWindowSize(g_window, &w, &h);
			glViewport(0, 0, w, h);

		
			projectionMatrix = perspective(radians(55.0f), float(w) / float(h), 1.0f, 10000.0f);
			viewMatrix = lookAt(cameraPosition, cameraPosition + cameraDirection, worldUp);
			
			//glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
			glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
			glClear(GL_COLOR_BUFFER_BIT);


			glUseProgram(temp_program);
			glEnable(GL_PROGRAM_POINT_SIZE);
			glDisable(GL_CULL_FACE);
			glDisable(GL_DEPTH_TEST);
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE);
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, particleTexture);

			labhelper::setUniformSlow(temp_program, "V", viewMatrix);
			labhelper::setUniformSlow(temp_program, "P", projectionMatrix);
			labhelper::setUniformSlow(temp_program, "screen_x", (float)w);
			labhelper::setUniformSlow(temp_program, "screen_y", (float)h);
			labhelper::setUniformSlow(temp_program, "time", currentTime);
			
			
			glBindVertexArray(part_vao);
			glDrawArrays(GL_POINTS, 0, NUMBER_OF_PARTICLES);
		
			
			

		}
		else if (ray_test_program_on) {
			glUseProgram(ray_program);

			labhelper::setUniformSlow(ray_program, "time", (int)currentTime);
			glDispatchCompute((GLuint)tex_w, (GLuint)tex_h, 1);
			// enable Z-buffering
			glDisable(GL_DEPTH_TEST);
			// enable backface culling
			glDisable(GL_CULL_FACE);



			// make sure writing to image has finished before read
			glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);




			SDL_GetWindowSize(g_window, &w, &h);
			glViewport(0, 0, w, h);		// Set viewport

			glClearColor(0.2, 0.2, 0.8, 1.0);						// Set clear color
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			glUseProgram(textTest);
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, tex_output);
			glBindVertexArray(VAO);
			glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
		

		}
		else {
			
			SDL_GetWindowSize(g_window, &w, &h);
			glViewport(0, 0, w, h);		// Set viewport
			// render to window
			glEnable(GL_DEPTH_TEST);
			// enable backface culling
			glEnable(GL_CULL_FACE);
			display();

			//projectionMatrix = perspective(radians(55.0f), float(w) / float(h), 1.0f, 10000.0f);
			/*for (int i = 0; i < 0; i++) {
				AABB2D box = getBoundingBox(viewMatrix * vec4(particlePositions[i], 1.0f), point_light_radius, -1.0f, projectionMatrix);


				//std::cout << "position : " << particlePositions[0].x << ", " << particlePositions[0].y << "," << particlePositions[0].z << "\n";

				vertices[0] = box.left.x;
				vertices[1] = box.bottom.y;

				vertices[3] = box.right.x;
				vertices[4] = box.bottom.y;

				vertices[6] = box.left.x;
				vertices[7] = box.top.y;

				vertices[9] = box.left.x;
				vertices[10] = box.top.y;

				vertices[12] = box.right.x;
				vertices[13] = box.bottom.y;

				vertices[15] = box.right.x;
				vertices[16] = box.top.y;

				//tileClassification(tiles, w, h, (int)(w/32), (int)(h/32), 32, 32, viewMatrix* vec4(particlePositions[0], 1.0f), 15.0f, -1.0f, projectionMatrix);
				glBindBuffer(GL_ARRAY_BUFFER, VBO2);
				glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

				glBindFramebuffer(GL_FRAMEBUFFER, 0);
				glViewport(0, 0, w, h);

				glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
				glUseProgram(simpleShader);
				glBindVertexArray(VAO2);
				glDrawArrays(GL_TRIANGLES, 0, 6);
				glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
				glBindVertexArray(0);
			}*/
		}
		
		// Render overlay GUI.
		gui();

		// Swap front and back buffer. This frame will now been displayed.
		SDL_GL_SwapWindow(g_window);

		// check events (keyboard among other)
		stopRendering = handleEvents();
	}
	
	// Free Models
	labhelper::freeModel(landingpadModel);
	labhelper::freeModel(cameraModel);
	labhelper::freeModel(fighterModel);
	labhelper::freeModel(sphereModel);
	labhelper::freeModel(cornellModel);
	// Shut down everything. This includes the window and all other subsystems.
	labhelper::shutDown(g_window);
	return 0;
}
