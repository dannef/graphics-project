#include "ParticleSystem.h"
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>
using namespace glm;


vec3 boxMin(0.2774f, -1.0012f, 0.464f);
vec3 boxMax(0.7444f, -0.5342f, -0.0028f);
float particleRadius = 1.0f;
float SqDistPointAABB(vec3 p, vec3 bmin, vec3 bmax)
{
	float sqDist = 0.0f;
	for (int i = 0; i < 3; i++) {
		// For each axis count any excess distance outside box extents
		float v = p[i];
		if (v < bmin[i]) sqDist += (bmin[i] - v) * (bmin[i] - v);
		if (v > bmax[i]) sqDist += (v - bmax[i]) * (v - bmax[i]);
	}
	return sqDist;
}
int TestSphereAABB(vec3 s, vec3 bmin, vec3 bmax)
{
	// Compute squared distance between sphere center and AABB
	float sqDist = SqDistPointAABB(s, bmin, bmax);
	// Sphere and AABB intersect if the (squared) distance
	// between them is less than the (squared) sphere radius
	return sqDist <= particleRadius * particleRadius;
}

void ParticleSystem::kill(int id)
{
	Particle p = particles.back();
	Particle y = particles[id];


	particles[id] = p;
	particles.pop_back();

}

void ParticleSystem::spawn(Particle particle)
{
	if (particles.size() < max_size) {
		particles.push_back(particle);
	}

}

void ParticleSystem::process_particles(float dt, int mode, int vel)
{
	for (unsigned i = 0; i < particles.size(); ++i) {
		particles[i].lifetime += dt;
		if ((particles[i].lifetime) >= particles[i].life_length)
			kill(i); // Kill dead particles!

	}
	for (unsigned i = 0; i < particles.size(); ++i) {
		float radius = 1.0f;
		// check intersection with blue wall
		if (mode) {
			if (particles[i].pos.x + radius > 50.0f) {
				//reflect(particles[i].pos, 
				particles[i].velocity.x = -particles[i].velocity.x;//reflect(particles[i].pos, vec3(-1.0f, 0.0f, 0.0f));//vec3(-15.0f, 0.0f, 0.0f);
				particles[i].pos.x -= (particles[i].pos.x + radius - 50.0f);

			}
			if (particles[i].pos.x - radius < -50.0f) {
				//reflect(particles[i].pos, 
				particles[i].velocity.x = -particles[i].velocity.x;
				particles[i].pos.x -= (particles[i].pos.x - radius + 50.0f);

			}
			if (particles[i].pos.y + radius > 70.0f) {
				//reflect(particles[i].pos, 
				particles[i].velocity.y = -particles[i].velocity.y;
				particles[i].pos.y -= (particles[i].pos.y + radius - 70.0f);

			}
			if (particles[i].pos.y - radius < -30.0f) {
				//reflect(particles[i].pos, 
				particles[i].velocity.y = -particles[i].velocity.y;
				particles[i].pos.y -= (particles[i].pos.y - radius + 30.0f);

			}

			if (particles[i].pos.z + radius > 50.0f) {
				//reflect(particles[i].pos, 
				particles[i].velocity.z = -particles[i].velocity.z;
				particles[i].pos.z -= (particles[i].pos.z + radius - 50.0f);

			}
			if (particles[i].pos.z - radius < -50.0f) {
				//reflect(particles[i].pos, 
				particles[i].velocity.z = -particles[i].velocity.z;
				particles[i].pos.z -= (particles[i].pos.z - radius + 50.0f);

			}
			if(vel)
				particles[i].pos += particles[i].velocity*0.01f;
		}
		else {
			if (particles[i].pos.y - radius < -30.0f) {
				//reflect(particles[i].pos, 
				particles[i].velocity.y = -particles[i].velocity.y;
				particles[i].pos.y -= (particles[i].pos.y - radius + 29.0f);

			}
			particles[i].pos += particles[i].velocity*0.04f;// Update alive particles!
			particles[i].pos.y += -4.0f * particles[i].lifetime;
			// If a particle collides with a wall, change its direction.*/
		}
	}

}

void ParticleSystem::set_max_particles(int phi)
{
	max_size = phi;
}
