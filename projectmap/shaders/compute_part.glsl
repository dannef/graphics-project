
#version 430
#extension GL_ARB_compute_shader : enable
#extension GL_ARB_shader_image_load_store : enable

layout(local_size_x = 1024) in; // defines size of local work-group.

/*
struct Particle {
  vec4 position;
  vec4 velocity;

};
*/
layout(std430, binding = 3) buffer pos_particle_data {
  vec4 positions[];
};

layout(std430, binding = 4) buffer vel_particle_data {
  vec4 velocities[];
};



vec4 sphere = vec4(0.0, 0.0, 0.0, 10005.0);
uniform vec3 attraction_point;
uniform bool repel;
uniform bool stop_particles;
vec3 bounce(vec3 vin, vec3 n){
  vec3 vout = reflect(vin, n);
  return vout;
}
vec3 bounce_sphere(vec3 p , vec3 v, vec4 s){
  vec3 n = normalize(p - s.xyz);
  return bounce(v,n);
  
}

bool is_inside_sphere(vec3 p, vec4 s){
  
  float r = length(p - s.xyz);
  return (r < s.w);
}

void main(){

  vec3 G = vec3(0.0, -9.8, 0.0);
  const float DT = 0.1;
  
  uint globalID = gl_GlobalInvocationID.x;

  vec3 position = positions[globalID].xyz;

  
  vec3 pull_direction = normalize(attraction_point - position);
  
  float dist = length(pull_direction);
  G = pull_direction;


  vec3 pp;
  vec3 vp;
 
  vec3 velocity = velocities[globalID].xyz;
  
  if(!stop_particles){
    vp = velocity + 2*G*DT;//0.1*pull_direction * dist;
    pp = position + velocity*DT + 0.2*G;

  }
  else{
    pp = position;
    vp = vec3(0.0);
  }
  if(repel)
    pp = position - vp*0.1;

  //vec3 vp = velocity + 2*G*0.5*dist*dist;
  /*
  if(is_inside_sphere(pp, sphere)){
    vp = bounce_sphere(position, velocity, sphere);
    pp = position + vp*DT + 0.5*DT*DT*G;

  }
  */
  positions[globalID].xyz = pp;
  velocities[globalID].xyz = vp;
}

/* Define a fixed size of the work to complete in termsof number of jobs, i.e.,
 * the count of work groups.OpenGL calls this the global work group.

 you write a compute shader to process any one job within a work group, i.e., 
colour one single pixel, in the shader you must also define how the global work-group
should be sub-divided into smaller local work groups of jobs.

You then set up shader storage buffers or images/textures, that the compute shader
can read from or write to. You may also use uniforms as per normal, i.e., create a texture
with the dimensions of a 2d Image.

Then on the CPU, you call glDispatchCompute(), and OpenGL will launch as many compute shader 
invocations 
*/

