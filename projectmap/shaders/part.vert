#version 420
layout(location = 0) in vec3 pos;

uniform mat4 V;
uniform mat4 P;

uniform float screen_x;
uniform float screen_y;

void main(){

  vec4 point_vs = V * vec4(pos, 1.0);

  vec4 point_ps = P * point_vs;

  vec4 proj_quad  = P  * vec4(1.0, 1.0, point_vs.z, point_vs.w);
    // Calculate the projected pixel size.
  vec2 proj_pixel   = vec2(screen_x, screen_y) * proj_quad.xy / proj_quad.w;
    // Use scale factor as sum of x and y sizes.
  float scale_factor = (proj_pixel.x+proj_pixel.y);

  gl_Position = point_ps;  
  
  gl_PointSize = scale_factor * .5;
}
  
