#version 420

// required by GLSL spec Sect 4.5.3 (though nvidia does not, amd does)
precision highp float;

///////////////////////////////////////////////////////////////////////////////
// Material
///////////////////////////////////////////////////////////////////////////////
uniform vec3 material_color;
uniform float material_reflectivity;
uniform float material_metalness;
uniform float material_fresnel;
uniform float material_shininess;
uniform float material_emission;

uniform int has_emission_texture;
layout(binding = 5) uniform sampler2D emissiveMap;

///////////////////////////////////////////////////////////////////////////////
// Environment
///////////////////////////////////////////////////////////////////////////////
layout(binding = 6) uniform sampler2D environmentMap;
layout(binding = 7) uniform sampler2D irradianceMap;
layout(binding = 8) uniform sampler2D reflectionMap;
uniform float environment_multiplier;

///////////////////////////////////////////////////////////////////////////////
// Light source
///////////////////////////////////////////////////////////////////////////////
uniform vec3 point_light_color = vec3(1.0, 1.0, 1.0);
uniform float point_light_intensity_multiplier = 50.0;

///////////////////////////////////////////////////////////////////////////////
// Constants
///////////////////////////////////////////////////////////////////////////////
#define PI 3.14159265359

///////////////////////////////////////////////////////////////////////////////
// Input varyings from vertex shader
///////////////////////////////////////////////////////////////////////////////
in vec2 texCoord;
in vec3 viewSpaceNormal;
in vec3 viewSpacePosition;

///////////////////////////////////////////////////////////////////////////////
// Input uniform variables
///////////////////////////////////////////////////////////////////////////////
uniform mat4 viewInverse;
uniform vec3 viewSpaceLightPosition;

///////////////////////////////////////////////////////////////////////////////
// Output color
///////////////////////////////////////////////////////////////////////////////
layout(location = 0) out vec4 fragmentColor;
layout(location = 1) out vec3 normalColor;
layout(location = 2) out vec3 reflectColor;
layout(location = 3) out vec3 csPositionBuffer;


vec3 calculateDirectIllumiunation(vec3 wo, vec3 n)
{
	///////////////////////////////////////////////////////////////////////////
	// Task 1.2 - Calculate the radiance Li from the light, and the direction
	//            to the light. If the light is backfacing the triangle, 
	//            return vec3(0); 
	///////////////////////////////////////////////////////////////////////////
	//
	//
	//pow((viewSpaceLightPosition.z - viewSpacePosition.z), 2))
	//float d = sqrt(pow((viewSpaceLightPosition.x - viewSpacePosition.x), 2)) + pow((viewSpaceLightPosition.y - viewSpacePosition.y), 2)) + pow((viewSpaceLightPosition.z - viewSpacePosition.z), 2)));
	
	//float d = distance(viewSpacePosition, viewSpaceLightPosition);
	//vec3 Li(0.0);
	vec3 Li;
	//vec3 wi = normalize(viewSpacePosition - viewSpaceLightPosition );
	vec3 wi = normalize( viewSpaceLightPosition - viewSpacePosition);
	//vec3 wi = normalize( -viewSpaceLightPosition- viewSpacePosition);
	
	if(dot(n, wi) > 0){
		float d = distance(viewSpaceLightPosition, viewSpacePosition);
		d = pow(d, 2);
		Li = point_light_intensity_multiplier * point_light_color * (1/d);
	}
	else
		return vec3(0.0);

	///////////////////////////////////////////////////////////////////////////
	// Task 1.3 - Calculate the diffuse term and return that as the result
	///////////////////////////////////////////////////////////////////////////
	vec3 diffuse_term = material_color * 1/PI * abs(dot(n, wi)) * Li ;

	///////////////////////////////////////////////////////////////////////////
	// Task 2 - Calculate the Torrance Sparrow BRDF and return the light 
	//          reflected from that instead
	///////////////////////////////////////////////////////////////////////////
	
	vec3 wh = normalize(wo + wi);
	float R0 = material_fresnel;
	float s = material_shininess;

	float F = R0 + (1-R0)*pow(1 - dot(wh, wi), 5);
	float D = ((s+2)/2*PI) *pow(dot(n, wh), s);

	//dot(n, wh)*dot(n, wo) / dot(wo, wh)
	//dot(n, wh)*dot(n, wi) / dot(wo, wh)
   
	float G = min(1, min(
	2*(dot(n, wh)*dot(n, wo)) / dot(wo, wh),
	2*(dot(n, wh)*dot(n, wi)) / dot(wo, wh)
	)
	);

	float brdf = F*D*G/(4*dot(n,wo)*dot(n,wi));

	///////////////////////////////////////////////////////////////////////////
	// Task 3 - Make your shader respect the parameters of our material model.
	///////////////////////////////////////////////////////////////////////////
	
	vec3 dielectric_term = brdf * dot(n, wi)*Li + (1-F)*diffuse_term;
	vec3 metal_term = brdf * material_color * dot(n, wi)*Li;
	vec3 microfacet_term = material_metalness * metal_term + (1 - material_metalness)*dielectric_term;

	return material_reflectivity * microfacet_term + (1 - material_reflectivity) * diffuse_term;
	//return brdf * dot(n, wi) * Li;
	//return vec3(G);
	//return vec3(F);
	//return diffuse_term;
	//return vec3(material_color);
}

vec3 calculateIndirectIllumination(vec3 wo, vec3 n)
{
	///////////////////////////////////////////////////////////////////////////
	// Task 5 - Lookup the irradiance from the irradiance map and calculate
	//          the diffuse reflection
	///////////////////////////////////////////////////////////////////////////
	
	// Get world space normal. 

	vec3 world_normal = vec3(viewInverse * vec4(n, 0.0));
	// Calculate the spherical coordinates of the direction
	float theta = acos(max(-1.0f, min(1.0f, world_normal.y)));
	float phi = atan(world_normal.z, world_normal.x);
	if (phi < 0.0f) phi = phi + 2.0f * PI;
	// Use these to lookup the color in the environment map
	vec2 lookup = vec2(phi / (2.0 * PI), theta / PI);
	vec3 li = environment_multiplier * texture(irradianceMap, lookup).xyz;
	


	vec3 diffuse_term = material_color * (1/PI) * li;
	//return diffuse_term;
	///////////////////////////////////////////////////////////////////////////
	// Task 6 - Look up in the reflection map from the perfect specular 
	//          direction and calculate the dielectric and metal terms. 
	///////////////////////////////////////////////////////////////////////////

	float R0 = material_fresnel;
	float s = material_shininess;
	// get the correct incoming light ray by taking the reflection of the outgoing ray. Uncertain if it should -wo or wo.
	vec3 wi = normalize(reflect(wo, n));
	// Get the world space reflection vector
	vec3 wr = normalize(vec3(viewInverse * vec4(wi, 0.0)));
	// Get half vector between wi and w0.
	vec3 wh = normalize(wi + wo);
	
	theta = acos(max(-1.0f, min(1.0f, wr.y)));
	phi = atan(wr.z, wr.x);
	if (phi < 0.0f) phi = phi + 2.0f * PI;
	// Use these to lookup the color in the environment map
	lookup = vec2(phi / (2.0 * PI), theta / PI);

	float F = R0 + (1-R0)*pow(1 - dot(wh, wi), 5);
	float roughness = sqrt(sqrt(2/(s+2)));

	li = environment_multiplier * textureLod(reflectionMap, lookup, roughness * 7.0).xyz;
	vec3 dielectric_term = F * li + (1 - F)*diffuse_term;
	vec3 metal_term = F * material_color * li;

	vec3 microfacet_term = material_metalness * metal_term + (1-material_metalness)*dielectric_term;

	return material_reflectivity * microfacet_term + (1 - material_reflectivity) * diffuse_term;
	//
	//return material_reflectivity * dielectric_term + diffuse_term;
	

}


void main()
{
	vec3 wo = -normalize(viewSpacePosition);
	vec3 n = normalize(viewSpaceNormal);

	// Direct illumination
	vec3 direct_illumination_term = calculateDirectIllumiunation(wo, n);

	// Indirect illumination
	vec3 indirect_illumination_term = calculateIndirectIllumination(wo, n);

	///////////////////////////////////////////////////////////////////////////
	// Add emissive term. If emissive texture exists, sample this term.
	///////////////////////////////////////////////////////////////////////////
	vec3 emission_term = material_emission * material_color;
	if (has_emission_texture == 1) {
		emission_term = texture(emissiveMap, texCoord).xyz;
	}

	fragmentColor.xyz =
		direct_illumination_term +
		indirect_illumination_term +
		emission_term;

	normalColor = normalize(viewSpaceNormal);

	reflectColor = vec3(material_reflectivity);

	csPositionBuffer = viewSpacePosition;
}
