#version 430

layout(binding = 0) uniform sampler2D particleTexture;

uniform float time;

out vec4 FragColor;
void main()
{

  FragColor = texture2D(particleTexture, gl_PointCoord);
  float red, green, blue;
  
  red   =  cos(time*1.4+1.5);
  green =  cos(time*1.6)*sin(time*0.3)*1.35;
  blue  =  sin(time*1.2)*1.5;
  
  FragColor.x =  0.1;
  FragColor.y =  0.1;
  FragColor.z =  23.9;
 
  
}
