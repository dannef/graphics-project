#version 420

layout(binding = 0) uniform sampler2D frameBufferTexture;

uniform int filterSize = 4;

/**
 * Samples a region of the frame buffer using gaussian filter weights to blur the image
 * as the kernel width is not that large, it doesnt produce a very large effect. Making it larger
 * is both tedious and expensive, for real time purposes a separable blur is preferable, but this
 * requires several passes.
 * takes as input the centre coordinate to sample around.
 */


layout(location = 0) out vec4 fragmentColor;
vec4 textureRect(in sampler2D tex, vec2 rectangleCoord)
{
	return texture(tex, rectangleCoord / textureSize(tex, 0));
}

vec3 blur(vec2 coord, int filterSize2)
{
	vec3 result = vec3(0.0);
        float weight = 1.0 / (filterSize2 * filterSize2);

        for (int i = -filterSize2 / 2; i <= filterSize2 / 2; ++i)
            for (int j = -filterSize2 / 2; j <= filterSize2 / 2; ++j)
            {
				result += weight * textureRect(frameBufferTexture, coord + vec2(i, j)).xyz;
            }

	return result;
}

void main(){
  vec2 texcoords = gl_FragCoord.xy/vec2(1920.0, 1080.0);
  vec2 texelSize = 1.0 / vec2(textureSize(frameBufferTexture, 0));
  float result = 0.0;
    for (int x = -2; x < 2; ++x) 
    {
        for (int y = -2; y < 2; ++y) 
        {
            vec2 offset = vec2(float(x), float(y)) * texelSize;
            result += texture(frameBufferTexture, (gl_FragCoord.xy/textureSize(frameBufferTexture, 0)) + offset).r;
        }
    }
    fragmentColor = vec4(result/(4.0 *4.0));

  
 

}
