#version 420
#define PI 3.14159265359
precision highp float;

uniform vec3 material_color;
uniform float material_reflectivity;
uniform float material_metalness;
uniform float material_fresnel;
uniform float material_shininess;
uniform float material_emission;

in vec2 texCoord;
in vec3 viewSpaceNormal;
in vec3 viewSpacePosition;

///////////////////////////////////////////////////////////////////////////////
// Output color
///////////////////////////////////////////////////////////////////////////////
layout(location = 0) out vec4 fragmentColor;
layout(location = 1) out vec3 normalColor;
layout(location = 2) out float reflectivity;
layout(location = 3) out vec3 csPositionBuffer;
layout(location = 4) out vec3 met_fres_shiny;
layout(location = 5) out float emission;

void main()

{
  fragmentColor.xyz = material_color;
  normalColor = normalize(viewSpaceNormal);
  csPositionBuffer = viewSpacePosition;
  reflectivity = material_reflectivity;  
  met_fres_shiny = vec3(material_metalness, material_fresnel, material_shininess);
  emission = material_emission;
  
}

