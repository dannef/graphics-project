#version 430
#extension GL_ARB_compute_shader : enable
#extension GL_ARB_shader_image_load_store : enable

#define MATERIAL_LAMBERTIAN 0



layout(local_size_x = 1, local_size_y = 1) in; // defines size of local work-group.
layout(rgba32f, binding = 0) uniform image2D img_buffer;

// The second layout qualifier defines the internal format of the image that we set up.
// Note the use of image2d uniform rather than texture sampler as this allows us to write to
// any pixel that we want.

// Based on http://raytracey.blogspot.com/2016/11/opencl-path-tracing-tutorial-2-path.html
// https://www.scratchapixel.com/lessons/3d-basic-rendering/ray-tracing-generating-camera-rays/generating-camera-rays
// http://www.realtimerendering.com/raytracing/Ray%20Tracing%20in%20a%20Weekend.pdf

uniform int time;


// https://thebookofshaders.com/10/
// Noise function.
float random (vec2 st) {
    return fract(sin(dot(st.xy,
                         vec2(12.9898,78.233)))*
        43758.5453123);
}

uint rand(inout uint state)
{
    uint x = state;
    x ^= x << 13;
    x ^= x >> 17;
    x ^= x << 15;
    state = x;
    return x;
}

// ------------------------------------------------------------------

float random_float_01(inout uint state)
{
    return (rand(state) & 0xFFFFFF) / 16777216.0f;
}

vec3 random_in_unit_sphere_(inout uint state){
  vec3 p;
  do{
    p = 2.0*vec3(random_float_01(state), random_float_01(state), random_float_01(state)) - vec3(1.0, 1.0, 1.0);
  }while(length(p)*length(p) >= 1.0);

  return p;
}


vec3 random_in_unit_sphere(inout uint state)
{
    float z = random_float_01(state) * 2.0f - 1.0f;
    float t = random_float_01(state) * 2.0f * 3.1415926f;
    float r = sqrt(max(0.0, 1.0f - z * z));
    float x = r * cos(t);
    float y = r * sin(t);
    vec3 res = vec3(x, y, z);
    res *= pow(random_float_01(state), 1.0 / 3.0);
    return res;
}

struct Ray {
  vec3 origin;
  vec3 direction;
};

struct Sphere {
  float radius;
  vec3 pos;
  vec3 emi;
  vec3 color;
};

struct Hit {
  
  vec3 position;
  vec3 normal;
  float t;
  int material_id;
};

bool intersect_sphere(float t_min, float t_max, Sphere sphere, Ray ray, out Hit hit_rec){

  vec3 ray_to_center = ray.origin - sphere.pos;
  float a = dot(ray.direction, ray.direction);
  float b = dot(ray_to_center, ray.direction);
  float c = dot(ray_to_center, ray_to_center) - sphere.radius*sphere.radius;
  float disc = b*b - a*c;

  if(disc > 0.0f){
    float temp_t = (-b - sqrt(disc))/a;
    if(temp_t > t_min && temp_t < t_max){
      hit_rec.position = ray.origin + temp_t * ray.direction;
      hit_rec.normal = (hit_rec.position - sphere.pos) / sphere.radius;
      hit_rec.t = temp_t;
      return true;
    }		    
        		    
    temp_t = (-b + sqrt(disc))/a;

    if(temp_t > t_min && temp_t < t_max){
      hit_rec.position = ray.origin + temp_t * ray.direction;
      hit_rec.normal = (hit_rec.position - sphere.pos) / sphere.radius;
      hit_rec.t = temp_t;
      return true;
    }
  }
  return false;
}



Ray create_cam_ray(vec2 pixel_coords, float width, float height){
  float aspect_ratio = width/height;
  float x = float(pixel_coords.x)/width;
  float y = float(pixel_coords.y)/height;


  // Transformation from raster space to w
  float fx2 = (x - 0.5f) * aspect_ratio;
  float fy2 = (y - 0.5f);
  
  vec3 pixel_pos = vec3(fx2, fy2, 0.0f);

  Ray ray;

  ray.origin = vec3(0.0f, 0.0f, 40.0f);
  // Pixel exists on the Z = 0 plane, thus we create a vector between the
  // plane and the camera position.
  ray.direction = normalize(pixel_pos - ray.origin);

  return ray;
}

Sphere s[10];

vec3 trace(Ray ray, inout uint state){
  vec3 Li = vec3(0.0f);
  vec3 path_throughput = vec3(1.0, 1.0, 1.0);
  vec3 current_ray;
  int max_bounces = 8;
  for(int i = 0; i < max_bounces; i++){
    // get the intersection for this ray.
    Hit rec;
    bool result = intersect_sphere(0.0001, 10000.0, s[0], ray, rec);
    //bool result = intersect_sphere(0.001f, 10000.0f, s[0], rec);

    if(rec.material_id == MATERIAL_LAMBERTIAN){
      current_ray = rec.position + rec.normal + random_in_unit_sphere_(state);
    }
    vec3 wi = normalize(current_ray);
    float cosine_term = abs(dot(wi, rec.normal));
    

    //Li += path_throughput * direct_illumination; // send shadow ray towards light
    //Li += path_throughput * emissive_light; // Of the material

    // Sample incoming direction wi and get the brdf and pdf so we can
    // calculate the path_throughput or rather the radiant exitance

    
    
  }
  return vec3(0);
}
void main(){

  

  uint g_state = gl_GlobalInvocationID.x * 1973 + gl_GlobalInvocationID.y * 9277 +uint(time)* 2699 | 1 ;
  
  // gl_GlobalInvocationID.xy tells us where in the work group space
  // our invocation is and we use this to tell which pixel we should be modifying.
  // we write our final color to this position in the output image.
  ivec2 pixel_coords = ivec2(gl_GlobalInvocationID.xy);
  float x = float(pixel_coords.x)/1280.0;
  float y = float(pixel_coords.y)/720.0;
  
  
  s[0].radius = .3f;
  s[0].pos = vec3(0.0f, 0.0f, 3.0f);
  s[0].color = vec3(0.9f, 0.3f, 0.0f);

  struct Hit rec;
  rec.t = 1e20;

  int nr_of_samples = 45;

  vec4 accum_color;
  vec4 color;
  for(int i = 0; i < nr_of_samples; i++){
    Ray ray = create_cam_ray(pixel_coords + random_float_01(g_state), 1280.0, 720.0);    

    bool result = intersect_sphere(0.0001, 10000.0, s[0], ray, rec);
    if (!result){
      color = vec4(x * 0.1f, y*0.3f, 0.3f, 1.0f);
    }
    else{
      vec3 hit_point = ray.origin + ray.direction * rec.t;
      vec3 normal = normalize(hit_point - s[0].pos);
      float cosine_factor = dot(normal, ray.direction) * -1.0f;
      color = vec4(normal*0.5 + vec3(0.5), 1.0f);
    }
    accum_color += color;
  }
  accum_color /= float(nr_of_samples);
  
  vec3 prev_color = imageLoad(img_buffer, pixel_coords).rgb;
  vec3 out_color = mix(color.rgb, prev_color, 0.99);
  
  //  imageStore(img_buffer, pixel_coords, vec4(color, 1.0f));
  imageStore(img_buffer, pixel_coords, vec4(out_color, 1.0f)/*vec4(normal*0.5 + vec3(0.5)*//*cosine_factor * sphere1.color*/);
 
   
}
