
#version 420

precision highp float;

layout(binding = 0) uniform sampler2D hyperbolic_depthBuffer;
layout(binding = 1) uniform sampler2D cs_normalBuffer;
layout(binding = 2) uniform sampler2D rotationTexture;

const int kernel_size = 16;
uniform mat4 inverseProjMatrix;
uniform mat4 projectionMatrix;
uniform vec3 hemisphereSamples[kernel_size];
const float radius = 1.0;

// Division by tilesize for the rotations.
const vec2 noiseScale = vec2(1920.0/4.0, 1080.0/4.0);

in vec2 texCoord;
float width = 1920;
float height= 1080;
layout(location = 0) out vec4 fragmentSSAO;


mat3 rotateZ(float rad) {
    float c = cos(rad);
    float s = sin(rad);
    return mat3(
        c, s, 0.0,
        -s, c, 0.0,
        0.0, 0.0, 1.0
    );
}

/**
* Helper function to sample with pixel coordinates, e.g., (511.5, 12.75)
* This functionality is similar to using sampler2DRect.
* TexelFetch only work with integer coordinates and do not perform bilinerar filtering.
*/
vec4 textureRect(in sampler2D tex, vec2 rectangleCoord)
{
	return texture(tex, rectangleCoord / textureSize(tex, 0));
}


// Computes one vector in the plane perpendicular to v
vec3 perpendicular(vec3 v)
{
	vec3 av = abs(v); 
	if (av.x < av.y)
		if (av.x < av.z) return vec3(0.0f, -v.z, v.y);
		else return vec3(-v.y, v.x, 0.0f);
	else
		if (av.y < av.z) return vec3(-v.z, 0.0f, v.x);
		else return vec3(-v.y, v.x, 0.0f);
}
vec3 homogenize(vec4 v) { return vec3((1.0 / v.w) * v); }



void main() {

  float fragmentDepth = textureRect(hyperbolic_depthBuffer, gl_FragCoord.xy).x;
  // We want to tile our noise, so first divide by dimensions then multiply by the size of tiles.
  //vec3 randomVec = texture(rotationTexture, gl_FragCoord.xy/vec2(1920.0, 1080.0) * noiseScale).xyz;
  //float randomAngle = textureRect(rotationTexture, gl_FragCoord.xy).x;
  vec3 randomVec = texture(rotationTexture, (gl_FragCoord.xy/vec2(1920.0, 1080)) * noiseScale).xyz; 
  vec3 vs_normal      = textureRect(cs_normalBuffer, gl_FragCoord.xy).xyz;
  float x = gl_FragCoord.x / width;
  float y = gl_FragCoord.y / height;


  vec4 ndc = vec4(x * 2.0 - 1.0,
		  y * 2.0 - 1.0,
		  fragmentDepth * 2.0 - 1.0,
		  1.0);


  vec4 reconstructedPos = inverseProjMatrix * ndc;
  reconstructedPos /= reconstructedPos.w;
  vec3 vs_pos = vec3(reconstructedPos);


  //vec3 vs_tangent = perpendicular(vs_normal);
  //vs_tangent = rotateZ(radians(randomAngle*360.0)) * vs_tangent;
  //vs_tangent  = normalize(rotate(vs_tangent, vec3(0.0, 0.0, 1.0), radians(randomAngle*360.0f)));
  //vec3 vs_tangent   = normalize(randomVec - (vs_normal) * dot(randomVec, normalize(vs_normal)));
  //vec4 tangent = rotation * vec4(vs_tangent, 1.0);
  vec3 vs_tangent = normalize(randomVec - vs_normal * dot(randomVec, vs_normal));
  vec3 vs_bitangent = cross(vs_normal, vs_tangent);
  
 
  mat3 tbn = mat3(vs_tangent, vs_bitangent, vs_normal); // local base
  //tbn = rotateZ(radians(randomAngle*360.0)) * tbn;
  int num_visible_samples = 0; 
  int num_valid_samples = 0; 
  for (int i = 0; i < kernel_size; i++) {
    // Project hemishere sample onto the local base
    
    vec3 s = (tbn * hemisphereSamples[i]);
    // compute view-space position of sample
    vec3 vs_sample_position = vs_pos + s * radius;

    // compute the ndc-coords of the sample
    vec4 sample_coords = projectionMatrix * vec4(vs_sample_position, 1.0);
    vec4 sample_coords_ndc = sample_coords/sample_coords.w;
    sample_coords /= sample_coords.w;
    sample_coords *= 0.5;
    sample_coords += 0.5;
    sample_coords.x *= width;
    sample_coords.y *= height;
    
    // Sample the depth-buffer at a texture coord based on the ndc-coord of the sample
    float blocker_depth = textureRect(hyperbolic_depthBuffer, sample_coords.xy).x;
   
    // Find the view-space coord of the blocker
    vec3 vs_blocker_pos = homogenize(inverseProjMatrix * 
					   vec4(sample_coords_ndc.xy, blocker_depth * 2.0 - 1.0, 1.0));
    
    // Check that the blocker is closer than kernel_size to vs_pos
    if(length(vs_pos - vs_blocker_pos) > radius)
       continue;
	// (otherwise skip this sample)

    if(fragmentDepth < blocker_depth)
      num_visible_samples += 1;
	// Check if the blocker pos is closer to the camera than our
	// fragment, otherwise, increase num_visible_samples

	
    num_valid_samples += 1;
  }
  
  float hemisphericalVisibility = float(num_visible_samples) / float(num_valid_samples);
  
  if (num_valid_samples == 0)
    hemisphericalVisibility = 1.0;

  
  fragmentSSAO = vec4(hemisphericalVisibility);

}

