#version 420

precision highp float;

layout(binding = 0) uniform sampler2D gColor;
layout(binding = 1) uniform sampler2D gDepth;
layout(binding = 2) uniform sampler2D gNormal;
layout(binding = 4) uniform sampler2D gPosition;
layout(binding = 3) uniform sampler2D gReflectivity;
layout(binding = 5) uniform sampler2D gMetalFresShine;
//layout(binding = 5) uniform sampler2D gMetalness;
//layout(binding = 6) uniform sampler2D gFresnel;
//layout(binding = 7) uniform sampler2D gShininess;
//layout(binding = 6) uniform sampler2D gEmission;
layout(binding = 7) uniform sampler2D gSkybox;
layout(binding = 11) uniform sampler2D SSAO;
//layout(binding = 12) uniform sampler2D light_colors;
//layout(binding = 6) uniform sampler2D light_positions;


uniform data{
  int size[1980];
  int offset[1980];
  int light_index_grid[1980*6];

};

uniform data2{
  int size2[1980];
  int offset2[1980];
  int light_index_grid2[1980*6];

};



///////////////////////////////////////////////////////////////////////////////
// Environment
///////////////////////////////////////////////////////////////////////////////
layout(binding = 8) uniform sampler2D environmentMap;
layout(binding = 9) uniform sampler2D irradianceMap;
layout(binding = 10) uniform sampler2D reflectionMap;
uniform float environment_multiplier;

///////////////////////////////////////////////////////////////////////////////
// Light source
///////////////////////////////////////////////////////////////////////////////
uniform vec3 point_light_color = vec3(1.0, 1.0, 1.0);
uniform float point_light_intensity_multiplier = 50.0;

uniform vec3 particleLights[500];
uniform vec3 particleColors[500];
uniform float active_particles;


uniform vec3 lightPositions[100];
uniform vec3 lightColors[100];

///////////////////////////////////////////////////////////////////////////////
// Constants
///////////////////////////////////////////////////////////////////////////////
#define PI 3.14159265359
float width = 1920.0;
float height = 1080.0;
///////////////////////////////////////////////////////////////////////////////
// Input varyings from vertex shader
///////////////////////////////////////////////////////////////////////////////
in vec2 texCoord;
// These two must be fetched from buffers now.
//in vec3 viewSpaceNormal;
//in vec3 viewSpacePosition;

///////////////////////////////////////////////////////////////////////////////
// Input uniform variables
///////////////////////////////////////////////////////////////////////////////
uniform mat4 viewInverse;
uniform vec3 viewSpaceLightPosition;
uniform mat4 inverseProjMatrix;
uniform mat4 viewMatrix;
uniform mat4 MVP;
//layout (location = 0) out vec4 fragmentColor;


out vec4 fragmentColor;



vec4 textureRect(in sampler2D tex, vec2 rectangleCoord)
{
	return texture(tex, rectangleCoord / textureSize(tex, 0));
}


vec3 calculateDirectIllumiunation(vec3 wo,
				  vec3 n,
				  vec3 viewSpaceLightPosition,
				  vec3 viewSpacePosition,
				  vec3 material_color,
				  float material_fresnel,
				  float material_shininess,
				  float material_metalness,
				  float material_reflectivity)
{
	///////////////////////////////////////////////////////////////////////////
 // Naive implementation, however, since most lights that I have generated will infact affect the geometry
  // we are not wasting any computation :).
  	//return vec3(material_color);
  return vec3(0);
}

vec3 calculateIndirectIllumination(vec3 wo,
				   vec3 n,
				   vec3 viewSpaceLightPosition,
				   vec3 viewSpacePosition,
				   vec3 material_color,
				   float material_fresnel,
				   float material_shininess,
				   float material_metalness,
				   float material_reflectivity,
				   float ssaoMultiplier)
{
	///////////////////////////////////////////////////////////////////////////
	// Task 5 - Lookup the irradiance from the irradiance map and calculate
	//          the diffuse reflection
	///////////////////////////////////////////////////////////////////////////
	
	// Get world space normal. 

	vec3 world_normal = vec3(viewInverse * vec4(n, 0.0));
	// Calculate the spherical coordinates of the direction
	float theta = acos(max(-1.0f, min(1.0f, world_normal.y)));
	float phi = atan(world_normal.z, world_normal.x);
	if (phi < 0.0f) phi = phi + 2.0f * PI;
	// Use these to lookup the color in the environment map
	vec2 lookup = vec2(phi / (2.0 * PI), theta / PI);
	vec3 li = environment_multiplier * texture(irradianceMap, lookup).xyz;
	


	vec3 diffuse_term = material_color * (1/PI) * li * ssaoMultiplier;
	//return diffuse_term;
	///////////////////////////////////////////////////////////////////////////
	// Task 6 - Look up in the reflection map from the perfect specular 
	//          direction and calculate the dielectric and metal terms. 
	///////////////////////////////////////////////////////////////////////////

	float R0 = material_fresnel;
	float s = material_shininess;
	// get the correct incoming light ray by taking the reflection of the outgoing ray. Uncertain if it should -wo or wo.
	vec3 wi = normalize(reflect(wo, n));
	// Get the world space reflection vector
	vec3 wr = normalize(vec3(viewInverse * vec4(wi, 0.0)));
	// Get half vector between wi and w0.
	vec3 wh = normalize(wi + wo);
	
	theta = acos(max(-1.0f, min(1.0f, wr.y)));
	phi = atan(wr.z, wr.x);
	if (phi < 0.0f) phi = phi + 2.0f * PI;
	// Use these to lookup the color in the environment map
	lookup = vec2(phi / (2.0 * PI), theta / PI);

	float F = R0 + (1-R0)*pow(1 - dot(wh, wi), 5);
	float roughness = sqrt(sqrt(2/(s+2)));

	li = environment_multiplier * textureLod(reflectionMap, lookup, roughness * 7.0).xyz;
	vec3 dielectric_term = F * li + (1 - F)*diffuse_term;
	vec3 metal_term = F * material_color * li;

	vec3 microfacet_term = material_metalness * metal_term + (1-material_metalness)*dielectric_term;

	return material_reflectivity * microfacet_term + (1 - material_reflectivity) * diffuse_term;
	

}


void main()
{

  
  /*
  // Get all parameters from textures.
  float d = textureRect(gDepth, gl_FragCoord.xy).x;

  float x = gl_FragCoord.xy / width;
  float y = gl_FragCoord.xy / height;

  vec4 ndc = (x * 2.0 - 1.0,
	      y * 2.0 - 1.0,
	      d * 2.0 - 1.0,
	      1.0);
  */

  float SSAOMult = textureRect(SSAO, gl_FragCoord.xy).x;
  vec3 viewSpaceNormal   = textureRect(gNormal,      gl_FragCoord.xy).xyz;
  vec3 viewSpacePosition = textureRect(gPosition,     gl_FragCoord.xy).xyz;

  if(viewSpacePosition.z >= 0.0){
    fragmentColor = textureRect(gSkybox, gl_FragCoord.xy);
    return;
  }
  vec3  mat_color        = textureRect(gColor,        gl_FragCoord.xy).xyz;
  float mat_reflect      = textureRect(gReflectivity, gl_FragCoord.xy).x;
  float mat_metal        = textureRect(gMetalFresShine, gl_FragCoord.xy).x;
  float mat_fresnel      = textureRect(gMetalFresShine, gl_FragCoord.xy).y;
  float mat_shiny        = textureRect(gMetalFresShine, gl_FragCoord.xy).z;
  //float mat_emission     = textureRect(gEmission,     gl_FragCoord.xy).x;

  vec3 wo = -normalize(viewSpacePosition);
  vec3 n = normalize(viewSpaceNormal);

  
  // Direct illumination
  vec3 direct_illumination_term = calculateDirectIllumiunation(wo,
							       n,
							       viewSpaceLightPosition,
							       viewSpacePosition,
							       mat_color,
							       mat_fresnel,
							       mat_shiny,
							       mat_metal,
							       mat_reflect);

  // Indirect illumination
  vec3 indirect_illumination_term = calculateIndirectIllumination(wo,
								  n,
								  viewSpaceLightPosition,
								  viewSpacePosition,
								  mat_color,
								  mat_fresnel,
								  mat_shiny,
								  mat_metal,
								  mat_reflect,
								  SSAOMult);

  ///////////////////////////////////////////////////////////////////////////
  // Add emissive term. If emissive texture exists, sample this term.
  ///////////////////////////////////////////////////////////////////////////
  /*	vec3 emission_term = material_emission * material_color;
	if (has_emission_texture == 1) {
	emission_term = texture(emissiveMap, texCoord).xyz;
	}*/

  fragmentColor.xyz =
    direct_illumination_term + indirect_illumination_term;// +
    // indirect_illumination_term;// +
    //emission_term;

}

/*

void main()
{
  // FragColor = vec4(0.0, 1.0, 0.0, 1.0);
  FragColor = vec4(1.0f, 0.5f, 0.2f, 1.0f);
}
*/
