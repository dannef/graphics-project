#version 420

precision highp float;

layout(binding = 0) uniform sampler2D gColor;
layout(binding = 1) uniform sampler2D gDepth;
layout(binding = 2) uniform sampler2D gNormals;
layout(binding = 3) uniform sampler2D gReflectivity;
layout(binding = 4) uniform sampler2D gMetalness;
layout(binding = 6) uniform sampler2D gFresnel;
layout(binding = 7) uniform sampler2D gShininess;
layout(binding = 8) uniform sampler2D gEmission;

layout (location = 0) out vec4 fragmentColor;


/**
* Helper function to sample with pixel coordinates, e.g., (511.5, 12.75)
* This functionality is similar to using sampler2DRect.
* TexelFetch only work with integer coordinates and do not perform bilinerar filtering.
*/
vec4 textureRect(in sampler2D tex, vec2 rectangleCoord)
{
	return texture(tex, rectangleCoord / textureSize(tex, 0));
}

void main()
{


  fragmentColor = textureRect(gEmission, gl_FragCoord.xy);


}
