#version 430

precision highp float;

uniform float radius = 15.0;
uniform int nr_of_tiles = 60;

// The sizes of the arrays, size and offset needs to be set manually whenever we change the tile
// size and resolution, this can be resolved by just using multiple SSBSs and using dynamic
// buffers for each one instead, i.e., we only keep on single array in each SSBS.
layout (std430, binding = 12) buffer shader_data
{
  
  vec4 pos[10000];

  int size[240];
  int offset[240];
  int light_index_grid[];
};	

layout(binding = 0) uniform sampler2D gColor;
layout(binding = 1) uniform sampler2D gDepth;
layout(binding = 2) uniform sampler2D gNormal;
layout(binding = 3) uniform sampler2D gReflectivity;
layout(binding = 4) uniform sampler2D gPosition;
layout(binding = 5) uniform sampler2D gMetalFresShine;
//layout(binding = 5) uniform sampler2D gMetalness;
//layout(binding = 6) uniform sampler2D gFresnel;
//layout(binding = 7) uniform sampler2D gShininess;
//layout(binding = 6) uniform sampler2D gEmission;
layout(binding = 7) uniform sampler2D gSkybox;
layout(binding = 11) uniform sampler2D SSAO;
//layout(binding = 12) uniform sampler2D light_colors;
//layout(binding = 6) uniform sampler2D light_positions;

///////////////////////////////////////////////////////////////////////////////
// Environment
///////////////////////////////////////////////////////////////////////////////
layout(binding = 8) uniform sampler2D environmentMap;
layout(binding = 9) uniform sampler2D irradianceMap;
layout(binding = 10) uniform sampler2D reflectionMap;
uniform float environment_multiplier;

///////////////////////////////////////////////////////////////////////////////
// Light source
///////////////////////////////////////////////////////////////////////////////
uniform vec3 point_light_color = vec3(1.0, 1.0, 1.0);
uniform float point_light_intensity_multiplier = 50.0;

uniform float active_particles;
uniform vec3 lightColors[50];

uniform int TILE_WIDTH;
uniform int TILE_HEIGHT;

uniform int width;
uniform int height;

///////////////////////////////////////////////////////////////////////////////
// Constants
///////////////////////////////////////////////////////////////////////////////
#define PI 3.14159265359


///////////////////////////////////////////////////////////////////////////////
// Input varyings from vertex shader
///////////////////////////////////////////////////////////////////////////////
in vec2 texCoord;
// These two must be fetched from buffers now.
//in vec3 viewSpaceNormal;
//in vec3 viewSpacePosition;

///////////////////////////////////////////////////////////////////////////////
// Input uniform variables
///////////////////////////////////////////////////////////////////////////////
uniform mat4 viewInverse;
uniform vec3 viewSpaceLightPosition;
uniform mat4 inverseProjMatrix;
uniform mat4 viewMatrix;
uniform mat4 MVP;
layout (location = 0) out vec4 fragmentColor;
/**
* Helper function to sample with pixel coordinates, e.g., (511.5, 12.75)
* This functionality is similar to using sampler2DRect.
* TexelFetch only work with integer coordinates and do not perform bilinerar filtering.
*/



vec4 textureRect(in sampler2D tex, vec2 rectangleCoord)
{
	return texture(tex, rectangleCoord / textureSize(tex, 0));
}


vec3 calculateDirectIllumiunation(vec3 wo,
				  vec3 n,
				  vec3 viewSpaceLightPosition,
				  vec3 viewSpacePosition,
				  vec3 material_color,
				  float material_fresnel,
				  float material_shininess,
				  float material_metalness,
				  float material_reflectivity)
{
	///////////////////////////////////////////////////////////////////////////
	// Task 1.2 - Calculate the radiance Li from the light, and the direction
	//            to the light. If the light is backfacing the triangle, 
	//            return vec3(0); 
	///////////////////////////////////////////////////////////////////////////
	//
	//
  // Naive implementation, however, since most lights that I have generated will infact affect the geometry
  // we are not wasting any computation :).

  int tileX = int(gl_FragCoord.x / TILE_WIDTH);
  int tileY = int(gl_FragCoord.y / TILE_HEIGHT);

  int tileNumX = int(width / TILE_WIDTH);
  int tileNumY = int(height / TILE_HEIGHT);

  int t;
  t = (tileY * tileNumX) + tileX;
  /*  
  if(tileY == 0)
    t = (tileY * tileNumX) + tileX;
  else
    t = ((tileY-1)*tileNumX) + tileX;
  */  
  // if(size[t] <= 0)
  //return vec3(0);



  vec3 Li;
  vec3 sum;
  for(int i = 0; i < size[t]; i++){
    
    // Get light position
    //vec3 lightpos = lightPositions[i];
    //vec4 vs_lightPos = viewMatrix * vec4(lightpos, 1.0);
    // 
    // viewSpaceLightPosition = vec3(vs_lightPos);
    /*vec3 cs_light_pos = textureRect(light_positions, vec2(i+0.5, 0.0)).xyz;



      cs_light_pos = cs_light_pos * 2.0 - 1.0;
    
      vec4 cs_l = vec4(cs_light_pos, 1.0);


      cs_l = inverseProjMatrix * cs_l;
      cs_l /= cs_l.w;*/
    //vec3 p = vec3(pos[i]);
    int offset_ = offset[t]+i;
    int light_id = light_index_grid[offset_];
   

    viewSpaceLightPosition = vec3(viewMatrix * pos[light_id]);
    //viewSpaceLightPosition = vec3(viewMatrix * vec4(particleLights[light_id], 1.0));
    //point_light_color = particleColors[i];
    //vec3 lightColor = lightColors[i];

    
    vec3 wi = normalize( viewSpaceLightPosition - viewSpacePosition);

    // Calculate Li for all lights.
    if(dot(n, wi) >= 0){    
      float d = distance(viewSpaceLightPosition, viewSpacePosition);
      float alpha = 1.0;

     
      alpha = 1-smoothstep(0, radius, d);
      
       d = pow(d, 2);
       
      //Li += point_light_intensity_multiplier * point_light_color * (1/d);
       // 1.5 is just a magic number for more intensity of lights.
       Li = point_light_intensity_multiplier * lightColors[light_id % 50] *(1/d)* alpha *1.5;
      //Li = point_light_intensity_multiplier * vec3(0.1, 0.2, 0.9) * (1/d);// * alpha;
      //Li = point_light_intensity_multiplier * vec3(c[i]) * (1/d);
      
      //Li = point_light_intensity_multiplier * textureRect(light_colors, vec2(i+0.5, 0.0)).xyz * (1/d);

	  
    }
    else
      continue;
    //return vec3(0.0);

    ///////////////////////////////////////////////////////////////////////////
    // Task 1.3 - Calculate the diffuse term and return that as the result
    ///////////////////////////////////////////////////////////////////////////
    vec3 diffuse_term = material_color * 1/PI * abs(dot(n, wi)) * Li ;

    ///////////////////////////////////////////////////////////////////////////
    // Task 2 - Calculate the Torrance Sparrow BRDF and return the light 
    //          reflected from that instead
    ///////////////////////////////////////////////////////////////////////////
	
    vec3 wh = normalize(wo + wi);
    float R0 = material_fresnel;
    float s = material_shininess;

    float F = R0 + (1-R0)*pow(1 - dot(wh, wi), 5);
    float D = ((s+2)/2*PI) *pow(dot(n, wh), s);

    //dot(n, wh)*dot(n, wo) / dot(wo, wh)
    //dot(n, wh)*dot(n, wi) / dot(wo, wh)
   
    float G = min(1, min(
			 2*(dot(n, wh)*dot(n, wo)) / dot(wo, wh),
			 2*(dot(n, wh)*dot(n, wi)) / dot(wo, wh)
			 )
		  );

    float brdf = F*D*G/(4*dot(n,wo)*dot(n,wi));

    ///////////////////////////////////////////////////////////////////////////
    // Task 3 - Make your shader respect the parameters of our material model.
    ///////////////////////////////////////////////////////////////////////////
	
    vec3 dielectric_term = brdf * dot(n, wi)*Li + (1-F)*diffuse_term;
    vec3 metal_term = brdf * material_color * dot(n, wi)*Li;
    vec3 microfacet_term = material_metalness * metal_term + (1 - material_metalness)*dielectric_term;

    sum += material_reflectivity * microfacet_term + (1 - material_reflectivity) * diffuse_term;
  }

  return sum;
  //return brdf * dot(n, wi) * Li;
  //return vec3(G);
  //return vec3(F);
  //return diffuse_term;
  //return vec3(material_color);
}

vec3 calculateIndirectIllumination(vec3 wo,
				   vec3 n,
				   vec3 viewSpaceLightPosition,
				   vec3 viewSpacePosition,
				   vec3 material_color,
				   float material_fresnel,
				   float material_shininess,
				   float material_metalness,
				   float material_reflectivity,
				   float ssaoMultiplier)
{
	///////////////////////////////////////////////////////////////////////////
	// Task 5 - Lookup the irradiance from the irradiance map and calculate
	//          the diffuse reflection
	///////////////////////////////////////////////////////////////////////////
	
	// Get world space normal. 

	vec3 world_normal = vec3(viewInverse * vec4(n, 0.0));
	// Calculate the spherical coordinates of the direction
	float theta = acos(max(-1.0f, min(1.0f, world_normal.y)));
	float phi = atan(world_normal.z, world_normal.x);
	if (phi < 0.0f) phi = phi + 2.0f * PI;
	// Use these to lookup the color in the environment map
	vec2 lookup = vec2(phi / (2.0 * PI), theta / PI);
	vec3 li = environment_multiplier * texture(irradianceMap, lookup).xyz;
	


	vec3 diffuse_term = material_color * (1/PI) * li * ssaoMultiplier;
	//return diffuse_term;
	///////////////////////////////////////////////////////////////////////////
	// Task 6 - Look up in the reflection map from the perfect specular 
	//          direction and calculate the dielectric and metal terms. 
	///////////////////////////////////////////////////////////////////////////

	float R0 = material_fresnel;
	float s = material_shininess;
	// get the correct incoming light ray by taking the reflection of the outgoing ray. Uncertain if it should -wo or wo.
	vec3 wi = normalize(reflect(wo, n));
	// Get the world space reflection vector
	vec3 wr = normalize(vec3(viewInverse * vec4(wi, 0.0)));
	// Get half vector between wi and w0.
	vec3 wh = normalize(wi + wo);
	
	theta = acos(max(-1.0f, min(1.0f, wr.y)));
	phi = atan(wr.z, wr.x);
	if (phi < 0.0f) phi = phi + 2.0f * PI;
	// Use these to lookup the color in the environment map
	lookup = vec2(phi / (2.0 * PI), theta / PI);

	float F = R0 + (1-R0)*pow(1 - dot(wh, wi), 5);
	float roughness = sqrt(sqrt(2/(s+2)));

	li = environment_multiplier * textureLod(reflectionMap, lookup, roughness * 7.0).xyz;
	vec3 dielectric_term = F * li + (1 - F)*diffuse_term;
	vec3 metal_term = F * material_color * li;

	vec3 microfacet_term = material_metalness * metal_term + (1-material_metalness)*dielectric_term;

	return material_reflectivity * microfacet_term + (1 - material_reflectivity) * diffuse_term;
	

}


void main()
{

  
  /*
  // Get all parameters from textures.
  float d = textureRect(gDepth, gl_FragCoord.xy).x;

  float x = gl_FragCoord.xy / width;
  float y = gl_FragCoord.xy / height;

  vec4 ndc = (x * 2.0 - 1.0,
	      y * 2.0 - 1.0,
	      d * 2.0 - 1.0,
	      1.0);
  */

  float SSAOMult = textureRect(SSAO, gl_FragCoord.xy).x;
  vec3 viewSpaceNormal   = textureRect(gNormal,      gl_FragCoord.xy).xyz;
  vec3 viewSpacePosition = textureRect(gPosition,     gl_FragCoord.xy).xyz;

  if(viewSpacePosition.z >= 0.0){
    fragmentColor = textureRect(gSkybox, gl_FragCoord.xy);
    return;
  }
  vec3  mat_color        = textureRect(gColor,        gl_FragCoord.xy).xyz;
  float mat_reflect      = textureRect(gReflectivity, gl_FragCoord.xy).x;
  float mat_metal        = textureRect(gMetalFresShine, gl_FragCoord.xy).x;
  float mat_fresnel      = textureRect(gMetalFresShine, gl_FragCoord.xy).y;
  float mat_shiny        = textureRect(gMetalFresShine, gl_FragCoord.xy).z;
  //float mat_emission     = textureRect(gEmission,     gl_FragCoord.xy).x;

  vec3 wo = -normalize(viewSpacePosition);
  vec3 n = normalize(viewSpaceNormal);

  
  // Direct illumination
  vec3 direct_illumination_term = calculateDirectIllumiunation(wo,
							       n,
							       viewSpaceLightPosition,
							       viewSpacePosition,
							       mat_color,
							       mat_fresnel,
							       mat_shiny,
							       mat_metal,
							       mat_reflect);

  // Indirect illumination
  vec3 indirect_illumination_term = calculateIndirectIllumination(wo,
								  n,
								  viewSpaceLightPosition,
								  viewSpacePosition,
								  mat_color,
								  mat_fresnel,
								  mat_shiny,
								  mat_metal,
								  mat_reflect,
								  SSAOMult);

  ///////////////////////////////////////////////////////////////////////////
  // Add emissive term. If emissive texture exists, sample this term.
  ///////////////////////////////////////////////////////////////////////////
  /*	vec3 emission_term = material_emission * material_color;
	if (has_emission_texture == 1) {
	emission_term = texture(emissiveMap, texCoord).xyz;
	}*/

  fragmentColor.xyz =
    direct_illumination_term + indirect_illumination_term;// +
    // indirect_illumination_term;// +
    //emission_term;

}
/*
vec3 calculateDirectIllumiunation(vec3 wo,
				  vec3 n,
				  vec3 viewSpaceLightPosition,
				  vec3 viewSpacePosition,
				  vec3 material_color,
				  float material_fresnel,
				  float material_shininess,
				  float material_metalness,
				  float material_reflectivity)
{
	///////////////////////////////////////////////////////////////////////////
	// Task 1.2 - Calculate the radiance Li from the light, and the direction
	//            to the light. If the light is backfacing the triangle, 
	//            return vec3(0); 
	///////////////////////////////////////////////////////////////////////////
	//
	//
  // Naive implementation, however, since most lights that I have generated will infact affect the geometry
  // we are not wasting any computation :).
  vec3 Li;
  vec3 sum;
  for(int i = 0; i < active_particles; i++){
    
    // Get light position
    //vec3 lightpos = lightPositions[i];
    //vec4 vs_lightPos = viewMatrix * vec4(lightpos, 1.0);
    // 
    // viewSpaceLightPosition = vec3(vs_lightPos);
    /*vec3 cs_light_pos = textureRect(light_positions, vec2(i+0.5, 0.0)).xyz;



    cs_light_pos = cs_light_pos * 2.0 - 1.0;
    
    vec4 cs_l = vec4(cs_light_pos, 1.0);


    //vec3 p = vec3(pos[i]);
    viewSpaceLightPosition = vec3(viewMatrix * pos[i]);
    //viewSpaceLightPosition = vec3(viewMatrix * vec4(particleLights[i], 1.0));
    //point_light_color = particleColors[i];
    //vec3 lightColor = lightColors[i];

    
    vec3 wi = normalize( viewSpaceLightPosition - viewSpacePosition);

    // Calculate Li for all lights.
    if(dot(n, wi) >= 0){    
      float d = distance(viewSpaceLightPosition, viewSpacePosition);
      d = pow(d, 2);
      //Li += point_light_intensity_multiplier * point_light_color * (1/d);
      Li = point_light_intensity_multiplier * lightColors[i] * (1/d);
      //Li = point_light_intensity_multiplier * vec3(c[i]) * (1/d);
      
      //Li = point_light_intensity_multiplier * textureRect(light_colors, vec2(i+0.5, 0.0)).xyz * (1/d);

	  
    }
    else
      continue;
      //return vec3(0.0);

    ///////////////////////////////////////////////////////////////////////////
    // Task 1.3 - Calculate the diffuse term and return that as the result
    ///////////////////////////////////////////////////////////////////////////
    vec3 diffuse_term = material_color * 1/PI * abs(dot(n, wi)) * Li ;

    ///////////////////////////////////////////////////////////////////////////
    // Task 2 - Calculate the Torrance Sparrow BRDF and return the light 
    //          reflected from that instead
    ///////////////////////////////////////////////////////////////////////////
	
    vec3 wh = normalize(wo + wi);
    float R0 = material_fresnel;
    float s = material_shininess;

    float F = R0 + (1-R0)*pow(1 - dot(wh, wi), 5);
    float D = ((s+2)/2*PI) *pow(dot(n, wh), s);

    //dot(n, wh)*dot(n, wo) / dot(wo, wh)
    //dot(n, wh)*dot(n, wi) / dot(wo, wh)
   
    float G = min(1, min(
			 2*(dot(n, wh)*dot(n, wo)) / dot(wo, wh),
			 2*(dot(n, wh)*dot(n, wi)) / dot(wo, wh)
			 )
		  );

    float brdf = F*D*G/(4*dot(n,wo)*dot(n,wi));

    ///////////////////////////////////////////////////////////////////////////
    // Task 3 - Make your shader respect the parameters of our material model.
    ///////////////////////////////////////////////////////////////////////////
	
    vec3 dielectric_term = brdf * dot(n, wi)*Li + (1-F)*diffuse_term;
    vec3 metal_term = brdf * material_color * dot(n, wi)*Li;
    vec3 microfacet_term = material_metalness * metal_term + (1 - material_metalness)*dielectric_term;

    sum += material_reflectivity * microfacet_term + (1 - material_reflectivity) * diffuse_term;
  }
  return sum;
}*/
