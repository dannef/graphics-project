#version 420 compatibility
in float life;
in vec4 particle_vs;

uniform float screen_x;
uniform float screen_y;
uniform mat4 inverseProjMatrix;
uniform mat4 projectionMatrix;
uniform mat4 inverseView;
uniform float w;
uniform float h;


layout(binding = 0) uniform sampler2D colortexture;
layout(binding = 1) uniform sampler2D gDepth;
layout(binding = 2) uniform sampler2D prevFrame;

float reconstructCSZ(float depthBufferValue, vec3 clipInfo) {
      return clipInfo[0] / (depthBufferValue * clipInfo[1] + clipInfo[2]);
}

vec3 computeClipInfo(float zn, float zf) { 
    if (zf == -100000.0) {
        return vec3(zn, -1.0f, +1.0f);
    } else {
        return vec3(zn  * zf, zn - zf, zf);
    }
}
float nearPlaneZ = -15.0;
float farPlaneZ = -10000.0;

void main()
{
  gl_FragColor = vec4(1.0);
  
    // Basse color.
    gl_FragColor = texture2D(colortexture,gl_PointCoord);
    vec4 fragDepth2 = texture2D(gDepth, gl_PointCoord);
    
    vec4 k = vec4(gl_PointCoord.xy * 2.0 -1.0 , fragDepth2.x * 2.0 - 1.0, 1.0);

    vec4 g = inverseProjMatrix * k;
    g /= g.w;
    float d = gl_FragDepth;
    
  
    vec4 projectedCoord = projectionMatrix * particle_vs;
    projectedCoord /= projectedCoord.w;

    projectedCoord.x = projectedCoord.x * 0.5 + 0.5;
    projectedCoord.x *= w;
    projectedCoord.y = projectedCoord.y * 0.5 + 0.5;
    projectedCoord.y *= h;

    vec4 fragDepth = texture(gDepth, projectedCoord.xy/textureSize(gDepth, 0));
    float csDepth = reconstructCSZ(fragDepth.x, computeClipInfo(nearPlaneZ, farPlaneZ));
      
    
    if(particle_vs.z < csDepth){
      discard;
      vec4 prevFrame = texture(prevFrame, projectedCoord.xy/textureSize(gDepth, 0));
      
      float fade = smoothstep(0, 1, abs(particle_vs.z - csDepth)*10.0);
      vec4 fragC = vec4(gl_FragColor.xyz * (1.0-life), gl_FragColor.w * (1.0-pow(life, 4.0))*0.5);

      gl_FragColor = mix(prevFrame, fragC, 0.5);
      
    }    // Make it darker the older it is.
    
    gl_FragColor.xyz *= (1.0-life);
    // Make it fade out the older it is, also all particles have a 
    // very low base alpha so they blend together.
    gl_FragColor.w = gl_FragColor.w * (1.0-pow(life, 4.0))*0.5;

  
    //gl_FragColor = vec4(fragDepth.x);
    //gl_FragColor = vec4(1.0, 0.0, 0.5, 1.0);
}
