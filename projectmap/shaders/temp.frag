// Working tracer.
bool traceScreenSpaceRay1
(vec3          csOrigin, 
 vec3          csDirection,            
 float         csZThickness,
 const in bool csZBufferIsHyperbolic,
 float         nearPlaneZ,
 float         farPlaneZ,
 float	       stride,
 float         jitterFraction,
 float         maxSteps,
 in float      maxRayTraceDistance,
 out vec2      hitPixel,
 int           which,
 out vec3      csHitPoint) {
   
    // Clip ray to a near plane in 3D (doesn't have to be *the* near plane, although that would be a good idea)
    float rayLength = ((csOrigin.z + csDirection.z * maxRayTraceDistance) > nearPlaneZ) ?
                        (nearPlaneZ - csOrigin.z) / csDirection.z :
                        maxRayTraceDistance;
    vec3 csEndPoint = csDirection * rayLength + csOrigin;

    // Project into screen space (-w/2, w/2, -h/2, h/2)
    vec4 H0 = projectionMatrix * vec4(csOrigin, 1.0);
    vec4 H1 = projectionMatrix * vec4(csEndPoint, 1.0);

    // There are a lot of divisions by w that can be turned into multiplications
    // at some minor precision loss...and we need to interpolate these 1/w values
    // anyway.
    //
    // Because the caller was required to clip to the near plane,
    // this homogeneous division (projecting from 4D to 2D) is guaranteed 
    // to succeed. 
    float k0 = 1.0 / H0.w;
    float k1 = 1.0 / H1.w;

    // Switch the original points to values that interpolate linearly in 2D
    vec3 Q0 = csOrigin * k0; 
    vec3 Q1 = csEndPoint * k1;

	// Screen-space endpoints
    vec2 P0 = H0.xy * k0;
    vec2 P1 = H1.xy * k1;

    // [Optional clipping to frustum sides here]

    // Initialize to off screen
    hitPixel = vec2(-1.0, -1.0);
    which = 0; // Only one layer

    // If the line is degenerate, make it cover at least one pixel
    // to avoid handling zero-pixel extent as a special case later
    P1 += vec2((distanceSquared(P0, P1) < 0.0001) ? 0.01 : 0.0);

    vec2 delta = P1 - P0;

    // Permute so that the primary iteration is in x to reduce
    // large branches later
    bool permute = false;
	if (abs(delta.x) < abs(delta.y)) {
		// More-vertical line. Create a permutation that swaps x and y in the output
		permute = true;

        // Directly swizzle the inputs
		delta = delta.yx;
		P1 = P1.yx;
		P0 = P0.yx;        
	}
    
	// From now on, "x" is the primary iteration direction and "y" is the secondary one

    float stepDirection = sign(delta.x);
    float invdx = stepDirection / delta.x;
    vec2 dP = vec2(stepDirection, invdx * delta.y);

    // Track the derivatives of Q and k, used to interpolate the 3D position.
    vec3  dQ = (Q1 - Q0) * invdx;
    float dk = (k1 - k0) * invdx;

    // Scale derivatives by the desired pixel stride
	dP *= stride; dQ *= stride; dk *= stride;

    // Offset the starting values by the jitter fraction
	P0 += dP * jitterFraction; Q0 += dQ * jitterFraction; k0 += dk * jitterFraction;

	// Slide P from P0 to P1, (now-homogeneous) Q from Q0 to Q1, and k from k0 to k1
    vec3 Q = Q0;
    float  k = k0;

    // We track the ray depth at +/- 1/2 pixel to treat pixels as clip-space solid 
    // voxels. Because the depth at -1/2 for a given pixel will be the same as at 
    // +1/2 for the previous iteration, we actually only have to compute one value 
    // per iteration.
    float prevZMaxEstimate = csOrigin.z;
    float stepCount = 0.0;
    // RayZMax is the closest Z value, rayZMin is furthest Z value.
    float rayZMax = prevZMaxEstimate, rayZMin = prevZMaxEstimate;
    // sceneZMax is the currently sampled depth from depth buffer.
    float sceneZMax = rayZMax + 1000;

    // P1.x is never modified after this point, so pre-scale it by 
    // the step direction for a signed comparison
    float end = P1.x * stepDirection;

    // We only advance the z field of Q in the inner loop, since
    // Q.xy is never used until after the loop terminates.
     // We never actually have to touch the xy, since we keep track of the
    // amount of steps we take we can adjust this later.

	for (vec2 P = P0;
        ((P.x * stepDirection) <= end) && 
        (stepCount < maxSteps) &&
        ((rayZMax < sceneZMax - csZThickness) ||
            (rayZMin > sceneZMax)) &&
        (sceneZMax != 0.0);
        P += dP, Q.z += dQ.z, k += dk, stepCount += 1.0) {
                
		hitPixel = permute ? P.yx : P;

        // The depth range that the ray covers within this loop
        // iteration.  Assume that the ray is moving in increasing z
        // and swap if backwards.  Because one end of the interval is
        // shared between adjacent iterations, we track the previous
        // value and then swap as needed to ensure correct ordering
        rayZMin = prevZMaxEstimate;

        // Compute the value at 1/2 pixel into the future
        rayZMax = (dQ.z * 0.5 + Q.z) / (dk * 0.5 + k);
	prevZMaxEstimate = rayZMax;
	
        if (rayZMin > rayZMax) { swap(rayZMin, rayZMax); }
	
	//hitPixel.y = 1080.0 - hitPixel.y;
	
	float x = (hitPixel.x * 0.5 + 0.5) * width;
	float y = (hitPixel.y * 0.5 + 0.5) * height;

	//y = 540.0 - y;
        // Camera-space z of the background
        sceneZMax = textureRect(depthTexture, vec2(x,y)).x;
	
        // This compiles away when csZBufferIsHyperbolic = false
        if (csZBufferIsHyperbolic) {
	  sceneZMax = reconstructCSZ(sceneZMax,computeClipInfo(nearPlaneZ, farPlaneZ));
        }
    }
	// pixel on ray
	// To get correct xy position we simply multiple the derivative by
	// how many steps we made and then divide Q by w again in order to
	// get the right point. We divided by W in order to do perspective correct
	// interpolation.
	
	Q.xy += dQ.xy * stepCount;
	csHitPoint = Q * (1.0 / k);

    // Matches the new loop condition:
    return (rayZMax >= sceneZMax - csZThickness) && (rayZMin <= sceneZMax);
}
