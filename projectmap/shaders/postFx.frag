#version 420

// required by GLSL spec Sect 4.5.3 (though nvidia does not, amd does)
precision highp float;

layout(binding = 0) uniform sampler2D frameBufferTexture;
layout(binding = 1) uniform sampler2D depthTexture;
layout(binding = 2) uniform sampler2D normalTexture;
layout(binding = 3) uniform sampler2D reflectTexture;
layout(binding = 4) uniform sampler2D positionTexture;
layout(binding = 5) uniform sampler1D rotationTexture;

uniform float time;
uniform int currentEffect;
uniform int filterSize;
uniform vec3 viewSpaceCameraPosition;

// Current resolution of window.
uniform int width;
uniform int height;

uniform mat4 projectionMatrix;
uniform mat4 inverseProjMatrix;

///////////////////////// BASIC SCREEN SPACE RAYTRACING VARIABLES ///////////////////////////////
uniform float basic_stepLength     = 2.0;
uniform int   basic_maxBinarySteps = 4;
uniform int   basic_maxDistance    = 50;
uniform int   basic_binarySearch   = 1;
////////////////////////////////////////////////////////////////////////////////////////////////

layout(location = 0) out vec4 fragmentColor;

uniform float zNear = 1.0;
uniform float zFar = 1000.0;

in vec2 texCoord;
// depthSample from depthTexture.r, for instance
float linearDepth(float depthSample)
{
    depthSample = 2.0 * depthSample - 1.0;
    float zLinear = 2.0 * zNear * zFar / (zFar + zNear - depthSample * (zFar - zNear));
    return zLinear;
}

bool traceScreenSpaceRay2(vec3 csRayOrigin,
     			  vec3 csRayDirection,
			  out vec2 hitPixel,
			  out vec3 hitPoint,
			  out float distanceTraveled);
bool traceScreenSpaceRay1
(vec3          csOrigin, 
 vec3          csDirection,            
 float         csZThickness,
 const in bool csZBufferIsHyperbolic,
 float         nearPlaneZ,
 float         farPlaneZ,
 float	       stride,
 float         jitterFraction,
 float         maxSteps,
 in float      maxRayTraceDistance,
 out vec2      hitPixel,
 int           which,
 out vec3      csHitPoint,
 out vec4      debug,
 out float     rayDistanceTraveled,
 bool          binarySearch);




/**
* Helper function to sample with pixel coordinates, e.g., (511.5, 12.75)
* This functionality is similar to using sampler2DRect.
* TexelFetch only work with integer coordinates and do not perform bilinerar filtering.
*/
vec4 textureRect(in sampler2D tex, vec2 rectangleCoord)
{
	return texture(tex, rectangleCoord / textureSize(tex, 0));
}

/**
 * Perturps the sampling coordinates of the pixel and returns the new coordinates
 * these can then be used to sample the frame buffer. The effect uses a sine wave to make us
 * feel woozy.
 */
vec2 mushrooms(vec2 inCoord);

/**
 * Samples a region of the frame buffer using gaussian filter weights to blur the image
 * as the kernel width is not that large, it doesnt produce a very large effect. Making it larger
 * is both tedious and expensive, for real time purposes a separable blur is preferable, but this
 * requires several passes.
 * takes as input the centre coordinate to sample around.
 */

vec3 blur(vec2 coord, int filterSize2)
{
	vec3 result = vec3(0.0);
        float weight = 1.0 / (filterSize2 * filterSize2);

        for (int i = -filterSize2 / 2; i <= filterSize2 / 2; ++i)
            for (int j = -filterSize2 / 2; j <= filterSize2 / 2; ++j)
            {
				result += weight * textureRect(frameBufferTexture, coord + vec2(i, j)).xyz;
            }

	return result;
}

/**
 * Simply returns the luminance of the input sample color.
 */
vec3 grayscale(vec3 rgbSample);

/**
 * Converts the color sample to sepia tone (by transformation to the yiq color space).
 */
vec3 toSepiaTone(vec3 rgbSample);

vec3 mosaic(vec2 coord);

float distanceSquared(vec2 a, vec2 b) { a -= b; return dot(a, a); }

vec3 PositionFromDepth(float depth) {
    float z = depth * 2.0 - 1.0;

    float x = gl_FragCoord.x/width;
    float y = gl_FragCoord.y/height;
    vec2 xy = vec2(x,y);
    vec4 clipSpacePosition = vec4(xy * 2.0 - 1.0, z, 1.0);
    vec4 viewSpacePosition = inverseProjMatrix * clipSpacePosition;

    // Perspective division
    viewSpacePosition /= viewSpacePosition.w;

    return viewSpacePosition.xyz;
}
vec3 binarySearch(vec3 csCurrentRayPos, vec3 csRayDir){

	vec4 projectedCoord;

	for(int i = 0; i < basic_maxBinarySteps; i++){

	// For each step, project coord into screenspace, check against depth buffer and see if we have a collision.
	  projectedCoord = projectionMatrix * vec4(csCurrentRayPos, 1.0);
	  projectedCoord /= projectedCoord.w;
	  // Move from NDC to [0,1] and then multiply by 
	  projectedCoord.x = projectedCoord.x * 0.5 + 0.5;
	  projectedCoord.x *= width;

	  projectedCoord.y = projectedCoord.y * 0.5 + 0.5;
	  projectedCoord.y *= height;


	  float csSampledCurrentRayDepth = textureRect(depthTexture, projectedCoord.xy).x;
	  vec3 p = PositionFromDepth(csSampledCurrentRayDepth);

	  // Then sample the background.


	  float depth = csCurrentRayPos.z - p.z; 
	  if(depth < 0.0)
	    csCurrentRayPos += csRayDir;
	  else
	    csCurrentRayPos -= csRayDir;

	  csRayDir *= 0.5;
	}


	projectedCoord = projectionMatrix * vec4(csCurrentRayPos, 1.0);
	projectedCoord /= projectedCoord.w;
	// Move from NDC to [0,1] and then multiply by 
	projectedCoord.x = projectedCoord.x * 0.5 + 0.5;
	projectedCoord.x *= width;

	projectedCoord.y = projectedCoord.y * 0.5 + 0.5;
	projectedCoord.y *= height;

	return vec3(projectedCoord.xy, 0.0);

}

vec4 rayMarch(vec3 csRayDir, vec3 csCurrentRayPos, out float distanceTraveled){
	
  csRayDir *= basic_stepLength;
	vec4 projectedCoord;
	float currentRayDepth;
	float maxRayLength;
	vec3 initialPos = csCurrentRayPos;


	//vec3 firstRayPos = currentRayPos;
	for(int i = 0; i < basic_maxDistance; i++){
		// Use backup environment map here.
	  distanceTraveled++; 

	  csCurrentRayPos += csRayDir;
		
	  // For each step, project coord into screenspace, check against depth buffer and see if we have a collision.
	  // this value is between -x/2 and x/2, do we hav eto fix this?
		
	  projectedCoord = projectionMatrix * vec4(csCurrentRayPos, 1.0);
		
	  // normalize to -1 and 1
	  projectedCoord /= projectedCoord.w;
		
	  // Move from NDC to [0,1] and then multiply by 
	  projectedCoord.x = projectedCoord.x * 0.5 + 0.5;
	  projectedCoord.x *= width;

	  projectedCoord.y = projectedCoord.y * 0.5 + 0.5;
	  projectedCoord.y *= height;

	  float csSampledCurrentRayDepth = textureRect(depthTexture, projectedCoord.xy).x;
	  vec3 p = PositionFromDepth(csSampledCurrentRayDepth);
	  
	  // Handle degenerate case where if we are outside buffer bounds on lookup.		
	  
	  if(csSampledCurrentRayDepth == 0.0)
	    return vec4(gl_FragCoord.xy, 0, 1.0);

	
	  //		float d = csSampledCurrentRayDepth - dd;
	  float d = csCurrentRayPos.z - p.z;
	
	  // Check first if we within reasonable distance between the two depths.
	  if((csRayDir.z - d) < 2.5 ){
	    // Since both depths are less than zero, since we are tracing in camera space, then if the sum is greater than 0
	    // then this means that we have gone past the object.
	    // return vec4(vec2(1080, 540), 0, 1.0);
	    //return vec4(projectedCoord.xy, 1.0, 1.0);	 
	    if(d <= 0.0){

	      vec3 result = binarySearch(csRayDir, csCurrentRayPos);
	      return vec4(result, 1.0);		
	      return vec4(projectedCoord.xy, 0.0, 1.0);	      	      


	    }
	  }
	}
	//	if(distanceTraveled >= 1000.0f)
	  //	  return vec4(0);
	return vec4(gl_FragCoord.xy, 0, 1.0);

}

void main()
{
	switch (currentEffect)
	{
	case 7:
	

	  fragmentColor = textureRect(frameBufferTexture, gl_FragCoord.xy);
	  
	  //fragmentColor = textureRect(frameBufferTexture, vec2(0.5, 0.5));

		break;
	case 0:
	  //fragmentColor = vec4(toSepiaTone(textureRect(frameBufferTexture, gl_FragCoord.xy).xyz), 1.0);
	  // fragmentColor = textureRect(frameBufferTexture, gl_FragCoord.xy);
	  float reflectVal_ = textureRect(reflectTexture, gl_FragCoord.xy).x;
	  if(reflectVal_ != 1.0){	    
	    fragmentColor = textureRect(frameBufferTexture, gl_FragCoord.xy);	    
	    break;
	  }

	 
	  vec3 csNormal_   = textureRect(normalTexture, gl_FragCoord.xy).xyz;
	  vec3 csPosition_ = textureRect(positionTexture, gl_FragCoord.xy).xyz;
	  // Add a distance in dir of normal, otherwise we got Z-fighting for some reason.
	  csPosition_ = csPosition_ + 1.0*normalize(csNormal_); 
	  vec3 csRayDir_ = normalize(reflect(csPosition_, normalize(csNormal_)));

	  float distance = 0.0f;
	  vec4 hit = rayMarch(csRayDir_, csPosition_, distance);

	  vec4 lookupHitXY = textureRect(frameBufferTexture, hit.xy);
	  //    vec4 lookupHitXY = textureRect(frameBufferTexture, projectedCoord.xy);
	  vec4 prevFrameXY = textureRect(frameBufferTexture, gl_FragCoord.xy); //vec4(blur(gl_FragCoord.xy, 15), 1.0);
	  // Fade based on the number of steps the reflection ray takes.
	  float stepFade = 1.0 - smoothstep(0, 1, (distance / basic_maxDistance));
	  // Reflection rays facing the camera, the more they face towards the cmaera, the more we fade.
	  float raysFacingCameraFade = 1.0 - smoothstep(0, 4, dot(csNormal_, csRayDir_));
	 
	  
	  float screenFadeVertical = 1.0 - smoothstep(0, height, hit.y);
	  float screenFadeHorizontal = 1.0 - smoothstep(0, width, hit.x);
	  // Multiply together to create an alpha.
	  float alpha = screenFadeVertical * stepFade * raysFacingCameraFade;

	  // 0.0 if sampling outside of buffer boundary.
	  if(lookupHitXY == vec4(0.0)){
	    fragmentColor = prevFrameXY;
	    break;
	  }

	  // fragmentColor = mix(textureRect(frameBufferTexture, hitPixel), textureRect(frameBufferTexture, gl_FragCoord.xy), stepFade);
	  fragmentColor = mix(prevFrameXY ,lookupHitXY, alpha);

	  break;
	case 2:
	  //fragmentColor = textureRect(frameBufferTexture, mushrooms(gl_FragCoord.xy));

	   float zNear = 1.0;    // TODO: Replace by the zNear of your perspective projection
	   float zFar  = 1000.0; // TODO: Replace by the zFar  of your perspective projection
	   float d = (textureRect(depthTexture, gl_FragCoord.xy).x);

	   d =  (2.0 * zNear) / (zFar + zNear - d * (zFar - zNear));	
	   fragmentColor = vec4(d);
		break;
	case 3:
	  fragmentColor = textureRect(normalTexture, gl_FragCoord.xy);
	  //fragmentColor = vec4(blur(gl_FragCoord.xy, filterSize), 1.0);
		break;
	case 4:
	  fragmentColor = vec4(textureRect(reflectTexture, gl_FragCoord.xy).x);
	  //		fragmentColor = vec4(grayscale(textureRect(frameBufferTexture, gl_FragCoord.xy).xyz), 1.0);
		break;
	case 5:
		// all at once
	  fragmentColor = vec4(toSepiaTone(blur(mushrooms(gl_FragCoord.xy), filterSize)), 1.0);
		break;
	case 6:
		fragmentColor = vec4(mosaic(gl_FragCoord.xy), 1.0); // place holder
		break;
	case 1:
		float reflectVal = textureRect(reflectTexture, gl_FragCoord.xy).x;
		if(reflectVal != 1.0){
		  
		  fragmentColor = textureRect(frameBufferTexture, gl_FragCoord.xy);

		  break;
		}
        
        
		vec3 csNormal   = textureRect(normalTexture, gl_FragCoord.xy).xyz;
		vec3 csPosition = textureRect(positionTexture, gl_FragCoord.xy).xyz;
		// Add a distance in dir of normal, otherwise we got Z-fighting for some reason.
		csPosition = csPosition + 5.5*normalize(csNormal); 
		vec3 csRayDir = normalize(reflect(normalize(csPosition), normalize(csNormal)));
	
		// The function returns a [-1, 1] values for coordinates. for hitPixel.
		vec2 hitPixel;
		vec3 hitPoint;
		int ok = 0; // some old stuff i dont use.
		vec4 col = vec4(0); // debug vec.
		float maxSteps = 600.0; 
		float steps = 0.0; // out steps value for fading
		float maxRayDist = 500.0;
		bool binarySearch = false;

		// The parameters need to be tuned for specific scenes, currently this works well for
		// the cornellModel, can probably find values which will work for most.
		bool intersect = traceScreenSpaceRay1(csPosition, // view space ray origin
					csRayDir,  // screen space ray direction
				        10.0, // thickness
					true, // we are using a 0,1 opengl depth buffer.
					-1.0, // near plane, should be negative.
					-10000.0,
					.01, // stride
					0.05, // jitter
					maxSteps, // max steps
					600.0, // max distance
					hitPixel, // 
					ok, 
					hitPoint,
					col,
					steps,
					binarySearch);

		/*TODO: 
		 * Binary search does not seem to work? Thin geometry?
		 * Possibly better blurring solutions.
		 */

		hitPixel.x = hitPixel.x *0.5+ 0.5;
		hitPixel.y = hitPixel.y *0.5+ 0.5;

		hitPixel.x *= width;
		hitPixel.y *= height;
  
		if(intersect){

		  // Hit point projection, if we want to use that..
		  vec4 projectedCoord = projectionMatrix * vec4(hitPoint, 1.0);
		  projectedCoord /= projectedCoord.w;
    
		  // Move from NDC to [0,1] and then multiply by 
		  projectedCoord.x = projectedCoord.x * 0.5 + 0.5;
		  projectedCoord.x *= width;
    
		  projectedCoord.y = projectedCoord.y * 0.5 + 0.5;
		  projectedCoord.y *= height;

		  //float depth = textureRect(positionTexture, projectedCoord.xy).z;
		  // Blur, does not seem to make much of a difference though.
		  vec4 lookupHitXY = textureRect(frameBufferTexture, hitPixel);
		  //    vec4 lookupHitXY = textureRect(frameBufferTexture, projectedCoord.xy);
		  vec4 prevFrameXY = textureRect(frameBufferTexture, gl_FragCoord.xy); //vec4(blur(gl_FragCoord.xy, 15), 1.0);
		  // Fade based on the number of steps the reflection ray takes.
		  float stepFade = 1.0 - smoothstep(0, 1, (steps / maxSteps));
		  // Reflection rays facing the camera, the more they face towards the cmaera, the more we fade.
		  float raysFacingCameraFade = 1.0 - smoothstep(0, 4, dot(csNormal, csRayDir));
		  // 740 is some arbitrary value less than 1080 so we get smooth fading whenever the y-value for a given
		  // hitpixel reaches high values.(1080 does not look good)
		  float screenFade = 1.0 - smoothstep(0, height, hitPixel.y);

		  //    screenFade = (smoothstep( 0.0, 1.0, maxDimension - screenFadeStart) / (1.0 - screenFadeStart));

		  // Multiply together to create an alpha.
		  float alpha = screenFade * stepFade * raysFacingCameraFade;

		  // 0.0 if sampling outside of buffer boundary.
		  if(lookupHitXY == vec4(0.0)){
		    fragmentColor = prevFrameXY;
		    return;
		  }

		  // fragmentColor = mix(textureRect(frameBufferTexture, hitPixel), textureRect(frameBufferTexture, gl_FragCoord.xy), stepFade);
		  fragmentColor = mix(prevFrameXY ,lookupHitXY, alpha);
		  //fragmentColor = vec4(texelFetch(frameBufferTexture, ivec2(hitPixel), 0).rgb, 1.0);
		  // fragmentColor = mix(textureRect(frameBufferTexture, hitPixel), col, 0.5);
		} 
		else
		  //fragmentColor = col;
		  fragmentColor = textureRect(frameBufferTexture, gl_FragCoord.xy);
    
  
		break;
  
	case 8:
	  fragmentColor = textureRect(frameBufferTexture, gl_FragCoord.xy);

	  break;
	}
}
/*********************************************************************************************/

/*********************************************************************************************/
vec3 computeClipInfo(float zn, float zf) { 
    if (zf == -100000.0) {
        return vec3(zn, -1.0f, +1.0f);
    } else {
        return vec3(zn  * zf, zn - zf, zf);
    }
}

/** Given an OpenGL depth buffer value on [0, 1] and description of the projection
    matrix's clipping planes, computes the camera-space (negative) z value.

    See also computeClipInfo in the .cpp file */ 
float reconstructCSZ(float depthBufferValue, vec3 clipInfo) {
      return clipInfo[0] / (depthBufferValue * clipInfo[1] + clipInfo[2]);
}


bool traceScreenSpaceRay1
(vec3          csOrigin, 
 vec3          csDirection,            
 float         csZThickness,
 const in bool csZBufferIsHyperbolic,
 float         nearPlaneZ,
 float         farPlaneZ,
 float	       stride,
 float         jitterFraction,
 float         maxSteps,
 in float      maxRayTraceDistance,
 out vec2      hitPixel,
 int           which,
 out vec3      csHitPoint,
 out vec4      debug,
 out float     steps,
 bool          binarySearch) {
   
    // Clip ray to a near plane in 3D (doesn't have to be *the* near plane, although that would be a good idea)
    float rayLength = ((csOrigin.z + csDirection.z * maxRayTraceDistance) > nearPlaneZ) ?
                        (nearPlaneZ - csOrigin.z) / csDirection.z :
                        maxRayTraceDistance;
    //rayLength = maxRayTraceDistance;
    vec3 csEndPoint = csDirection * rayLength + csOrigin;

    // Project into screen space (-w/2, w/2, -h/2, h/2)
    vec4 H0 = projectionMatrix * vec4(csOrigin, 1.0);
    vec4 H1 = projectionMatrix * vec4(csEndPoint, 1.0);

    // There are a lot of divisions by w that can be turned into multiplications
    // at some minor precision loss...and we need to interpolate these 1/w values
    // anyway.
    //
    // Because the caller was required to clip to the near plane,
    // this homogeneous division (projecting from 4D to 2D) is guaranteed 
    // to succeed. 
    float k0 = 1.0 / H0.w;
    float k1 = 1.0 / H1.w;

    // Switch the original points to values that interpolate linearly in 2D, this is by division with k.
    vec3 Q0 = csOrigin * k0; 
    vec3 Q1 = csEndPoint * k1;


    // Screen-space endpoints
    vec2 P0 = H0.xy * k0;
    vec2 P1 = H1.xy * k1;

    // [Optional clipping to frustum sides here]

    // Initialize to off screen
    hitPixel = vec2(-1.0, -1.0);
    which = 0; // Only one layer

    // If the line is degenerate, make it cover at least one pixel
    // to avoid handling zero-pixel extent as a special case later
    P1 += vec2((distanceSquared(P0, P1) < 0.0001) ? 0.01 : 0.0);

    // We want to find out the largest delta, x or y.
    vec2 delta = P1 - P0;

    // Permute so that the primary iteration is in x to reduce
    // large branches later, we want to iterate along the largest delta
    // in order to have more "even" samples.
    bool permute = false;
	if (abs(delta.x) < abs(delta.y)) {
		// More-vertical line. Create a permutation that swaps x and y in the output
		permute = true;

        // Directly swizzle the inputs
		delta = delta.yx;
		P1 = P1.yx;
		P0 = P0.yx;        
	}
    
    // From now on, "x" is the primary iteration direction and "y" is the secondary one
    float stepDirection = sign(delta.x);
    float invdx = stepDirection / delta.x;
    vec2 dP = vec2(stepDirection, invdx * delta.y);

    // Track the derivatives of Q and k, used to interpolate the 3D position.
    vec3  dQ = (Q1 - Q0) ;
    float dk = (k1 - k0) ;

    // Scale derivatives by the desired pixel stride
    dP *= stride; dQ *= stride; dk *= stride;

    // Offset the starting values by the jitter fraction
    P0 += dP * jitterFraction; Q0 += dQ * jitterFraction; k0 += dk * jitterFraction;

    // Slide P from P0 to P1, (now-homogeneous) Q from Q0 to Q1, and k from k0 to k1
    vec3 Q = Q0;
    float  k = k0;

    // We track the ray depth at +/- 1/2 pixel to treat pixels as clip-space solid 
    // voxels. Because the depth at -1/2 for a given pixel will be the same as at 
    // +1/2 for the previous iteration, we actually only have to compute one value 
    // per iteration.
    float prevZMaxEstimate = csOrigin.z;
    float stepCount = 0.0;
    // RayZMax is the closest Z value, rayZMin is furthest Z value.
    float rayZMax = prevZMaxEstimate, rayZMin = prevZMaxEstimate;
    // sceneZMax is the currently sampled depth from depth buffer.
    float sceneZMax = rayZMax + 100.0;

    // P1.x is never modified after this point, so pre-scale it by 
    // the step direction for a signed comparison
    float end = P1.x * stepDirection;

    // We only advance the z field of Q in the inner loop, since
    // Q.xy is never used until after the loop terminates.
     // We never actually have to touch the xy, since we keep track of the
    // amount of steps we take we can adjust this later.

    bool intersect = false;
    for (vec2 P = P0;
	 ((P.x * stepDirection) <= end) && 
	   (stepCount < maxSteps) &&
	   ((rayZMax < sceneZMax - csZThickness) ||
            (rayZMin > sceneZMax)) &&
	   (sceneZMax != 0.0);
	 P += dP, Q.z += dQ.z * invdx, k += dk*invdx, stepCount += 1.0) {

      Q.xy += stepCount * dQ.xy;
      
       
      hitPixel = permute ? P.yx : P;
      //hitPixel = permute ? vec2(Q.yx) : vec2(Q);
      // The depth range that the ray covers within this loop
      // iteration.  Assume that the ray is moving in increasing z
      // and swap if backwards.  Because one end of the interval is
      // shared between adjacent iterations, we track the previous
      // value and then swap as needed to ensure correct ordering
      rayZMin = prevZMaxEstimate;
      
      // Compute the value at 1/2 pixel into the future
      rayZMax = (dQ.z * 0.5 + Q.z) / (dk * 0.5 + k);
      // rayZMax = (Q.z) / (k);
      prevZMaxEstimate = rayZMax;
      
      if (rayZMin > rayZMax) {
	     float temp = rayZMin;
	     rayZMin = rayZMax;
	     rayZMax = temp;
      }

      // hitPixel is -1,1 range, multiply out and then by width/height.
      float x = (hitPixel.x * 0.5 + 0.5) * width;
      float y = (hitPixel.y * 0.5 + 0.5) * height;
            
      // Camera-space z of the background
      sceneZMax = textureRect(depthTexture, vec2(x,y)).x;

      // This compiles away when csZBufferIsHyperbolic = false
      // We use an opengl [0,1] buffer, i.e., a hyperbolic buffer.
      if (csZBufferIsHyperbolic) {
	sceneZMax = reconstructCSZ(sceneZMax,computeClipInfo(nearPlaneZ, farPlaneZ));
      }
    }
    
    // To get correct xy position we simply multiple the derivative by
    // how many steps we made and then divide Q by w again in order to
    // get the right point. We divided by W in order to do perspective correct
    // interpolation.
    Q.xy += dQ.xy * stepCount;
    steps = stepCount;
    csHitPoint = Q * (1.0 / k);
    intersect = (rayZMax >= sceneZMax - csZThickness) && (rayZMin <= sceneZMax);

    
	// Do binary search;	
	if(intersect && binarySearch){
	  float strideBinary = stride;

	  
	  for(float j = 0; j < 2.0; j++){
	    
	    dQ.z *= strideBinary;
	    dP *= strideBinary;
	    dk *= strideBinary;

	    //	    P0 += dP;  
	    Q.z += dQ.z;
	    k += dk;

	    rayZMin = prevZMaxEstimate;
	    
	    // Compute the value at 1/2 pixel into the future
	    rayZMax = (dQ.z * -0.5 + Q.z) / (dk * -0.5 + k);
	    prevZMaxEstimate = rayZMax;
	    if (rayZMin > rayZMax) {
	     float temp = rayZMin;
	     rayZMin = rayZMax;
	     rayZMax = temp;
	    }


	     
	    hitPixel = permute ? P0.yx : P0;
	     
	    float x = (hitPixel.x * 0.5 + 0.5) * width;
	    float y = (hitPixel.y * 0.5 + 0.5) * height;
	     

	    // Camera-space z of the background
	    sceneZMax = textureRect(depthTexture, vec2(x,y)).x;
	     
	    // This compiles away when csZBufferIsHyperbolic = false
	    if (csZBufferIsHyperbolic) {
	      sceneZMax = reconstructCSZ(sceneZMax,computeClipInfo(nearPlaneZ, farPlaneZ));
	    }
	    strideBinary *= .5;
	    strideBinary = (rayZMax >= sceneZMax - csZThickness) && (rayZMin <= sceneZMax)
	      ? strideBinary : -strideBinary;       
	      }	  
	}
	return intersect;
}

vec3 toSepiaTone(vec3 rgbSample)
{
	//-----------------------------------------------------------------
	// Variables used for YIQ/RGB color space conversion.
	//-----------------------------------------------------------------
	vec3 yiqTransform0 = vec3(0.299, 0.587, 0.144);
	vec3 yiqTransform1 = vec3(0.596,-0.275,-0.321);
	vec3 yiqTransform2 = vec3(0.212,-0.523, 0.311);

	vec3 yiqInverseTransform0 = vec3(1, 0.956, 0.621);
	vec3 yiqInverseTransform1 = vec3(1,-0.272,-0.647);
	vec3 yiqInverseTransform2 = vec3(1,-1.105, 1.702);

	// transform to YIQ color space and set color information to sepia tone
	vec3 yiq = vec3(dot(yiqTransform0, rgbSample), 0.2, 0.0);

	// inverse transform to RGB color space
	vec3 result = vec3(dot(yiqInverseTransform0, yiq), dot(yiqInverseTransform1, yiq), dot(yiqInverseTransform2, yiq));
	return result;
}


vec2 mushrooms(vec2 inCoord)
{
	return inCoord + vec2(cos(time * 4.3127 + inCoord.x / 9.0) * 15.0, 0.0);
}


vec3 grayscale(vec3 rgbSample)
{
	return vec3(rgbSample.r * 0.2126 + rgbSample.g * 0.7152 + rgbSample.b * 0.0722);
}


vec3 mosaic(vec2 coord)
{
	return textureRect(frameBufferTexture, coord - vec2(mod(coord.x, 25), mod(coord.y, 25))).xyz;
}

