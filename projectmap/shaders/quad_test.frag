#version 430                                                               

in vec2 texCoord;                                                                 
uniform sampler2D img;                                                      
layout(location = 0) out vec4 fragmentColor;                                                                
                                                                            
void main () {                                                              
  fragmentColor = texture2D(img, texCoord);

}
