#version 420

layout(location = 0) in vec3 position;

void main() 
{
	// gl_position is the actual position of the vertex in 3d space.
	gl_Position = vec4(position,1.0);

}
