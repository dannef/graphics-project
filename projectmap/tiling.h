#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>
using namespace glm;

#include <iostream>


class Tile {
public:
	std::vector<uint32_t> lights;

	Tile() {
	}
};

class AABB2D {
public:
	// Members
	vec4 bounds;
	vec3 left;
	vec3 right;
	vec3 bottom;
	vec3 top;
	// Ctor/Dtor
	AABB2D() : bounds(0.0f) {}
	explicit AABB2D(float left, float bottom, float right, float top) : bounds(left, bottom, right, top) {}

	// Methods
};

	vec3 project(mat4 P, vec3 Q) {
		vec4 v = P * vec4(Q, 1.0f);
		//std::cout << v.x << ", " << v.y << ", " << v.z << ", " << v.w << "\n";
		v = v / v.w;
		
		return vec3(v);
	}
	void getBoundsForAxis(vec3 a, const vec3& center, float radius, float nearZ, vec3 &U, vec3 &L) {

		const vec2 c(dot(a, center), center.z); // C in the a-z frame
		vec2 bounds[2]; // In the a-z reference frame
		const float tSquared = dot(c, c) - (radius*radius);
		//std::cout << sqrt(tSquared);
		const bool cameraInsideSphere = (tSquared <= 0);
		float distToCenter = length(c);
		float angle = asin(radius / distToCenter);

		mat2 clockWiserot = mat2(cos(angle), sin(angle),
			-sin(angle), cos(angle));

		vec2 centerVecNormalized = normalize(c);

		
		vec2 tDir = clockWiserot * centerVecNormalized;
		
		vec2 T = tDir * sqrt(tSquared);
		
		// Project to -1 plane.
		vec2 Tprime = T * (1 / -T.y);

		//std::cout << "T-prime pos: " << Tprime.x << ", " << Tprime.y << "\n";
		mat2 counterclockWiserot = mat2(cos(angle), -sin(angle),
			sin(angle), cos(angle));

		tDir = counterclockWiserot * centerVecNormalized;

		vec2 B = tDir * sqrt(tSquared);

		vec2 Bprime = B * (1 / -B.y);
		
		// Transform back to camera space
		L = Bprime.x * a; L.z = Bprime.y;
		U = Tprime.x * a; U.z = Tprime.y;

		//std::cout << "L pos: " << L.x << ", " << L.y << ", " << L.z << "\n";
		//std::cout << "U pos: " << U.x << ", " << U.y << ", " << U.z << "\n";
	}


	/** Center is in camera space */
	AABB2D getBoundingBox(const vec3 &center, float radius, float nearZ, mat4 P) {

		vec3 right, left, top, bottom;
		getBoundsForAxis(vec3(1.0f, 0.0f, 0.0f), center, radius, nearZ, right, left);
		getBoundsForAxis(vec3(0.0f, 1.0f, 0.0f), center, radius, nearZ, top, bottom);
		
		AABB2D p;

		p.left   = project(P, left);
		p.bottom = project(P, bottom);
		p.right  = project(P, right);
		p.top    = project(P, top);

		/*float l = project(P, left).x;
		float b = project(P, bottom).y;
		float r = project(P, right).x;
		float t = project(P, top).y;*/

		p.bounds = vec4(p.left.x, p.bottom.y, p.right.x, p.top.y);
		return p;
	

	}



/** Center is in camera space */

void tileClassification(int lightId, std::vector<Tile> &tiles, int resX, int resY, int tileNumX, int tileNumY, int tileWidth, int tileHeight,
	vec3 center, float radius, float nearZ, mat4 &projMatrix) {
	AABB2D box = getBoundingBox(center, radius, -1.0f, projMatrix);
	box.bounds = (box.bounds + 1.0f) * 0.5f;

	box.bounds.x = box.bounds.x * resX;
	box.bounds.y = box.bounds.y * resY;
	box.bounds.z = box.bounds.z * resX;
	box.bounds.w = box.bounds.w * resY;
	int minTileX = glm::max(0, (int)(box.bounds.x / tileWidth));
	int maxTileX = glm::min(tileNumX-1, (int)(box.bounds.z / tileWidth));

	int minTileY = glm::max(0, (int)(box.bounds.y / tileHeight));
	int maxTileY = glm::min(tileNumY, (int)(box.bounds.w / tileHeight));


	if ((minTileX > maxTileX) || (maxTileX < 0) || (minTileY > maxTileY) || (maxTileY < 0)) {
		
		return;
	}
	for (int i = minTileX; i <= maxTileX; i++) {
		for (int j = minTileY; j <= maxTileY; j++) {

			tiles[j*tileNumX + i].lights.push_back(lightId);

		}
	}
	
}

void create_tile_lists(std::vector<Tile> &tiles, std::vector<uint32_t> &light_grid_offset, std::vector<uint32_t> &light_grid_size, std::vector<uint32_t> &light_index){
	int current_index = 0;
	int tile = 0;
	
	for (Tile t : tiles) {
		light_grid_offset[tile] = current_index;
		
		
		if (!t.lights.empty()) {

			for (int i : t.lights) {
				light_index.push_back(i);
			}
			current_index += t.lights.size();
			
		}
		light_grid_size[tile] = t.lights.size();

		
		tile++;
	}

}



	/*
	void getBoundsForAxis(vec3 a, const vec3& center, float radius, float nearZ, vec3 &U, vec3 &L) {

		const vec2 c(dot(a, center), center.z); // C in the a-z frame
		vec2 bounds[2]; // In the a-z reference frame
		const float tSquared = dot(c, c) - (radius*radius);
		//std::cout << sqrt(tSquared);
		const bool cameraInsideSphere = (tSquared <= 0);

		// (cos, sin) of angle theta between c and a tangent vector
		vec2& v = cameraInsideSphere ?
			vec2(0.0f, 0.0f) :
			vec2(sqrt(tSquared), radius) * (float)c.length();
		// Does the near plane intersect the sphere?
		const bool clipSphere = (c.y + radius >= nearZ);
		// Square root of the discriminant; NaN (and unused)
		// if the camera is in the sphere
		float k = sqrt((radius*radius) - (nearZ - c.y) * (nearZ - c.y));
		for (int i = 0; i < 2; ++i) {
			if (!cameraInsideSphere) {
				mat2 rot = mat2(v.x, -v.y,
					v.y, v.x);
				bounds[i] = rot * c * v.x;
			}
			const bool clipBound =
				cameraInsideSphere || (bounds[i].y > nearZ);
			if (clipSphere && clipBound) {
				bounds[i] = vec2(c.x + k, nearZ);
				std::cout << cameraInsideSphere;
			}
			// Set up for the lower bound
			v.y = -v.y; k = -k;
		}
		// Transform back to camera space
		L = bounds[1].x * a; L.z = bounds[1].y;
		U = bounds[0].x * a; U.z = bounds[0].y;
	}*/